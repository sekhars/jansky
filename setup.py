from setuptools.extension import Extension
from setuptools import setup, find_packages
from Cython.Build import cythonize
import Cython.Compiler.Options
from setuptools.extension import Extension
import numpy

ext_modules = [
        Extension(
            "*", ['jansky/lib/*.pyx'],
            extra_compile_args=['-fopenmp', '-march=native'],
            extra_link_args=['-fopenmp'],
        )
    ]

setup (
    name='jansky',
    version='0.20',
    packages=find_packages(),
    include_package_data=True,
    ext_modules=cythonize(ext_modules,
        compiler_directives={'boundscheck':False, 'wraparound':False,
            'embedsignature':True, 'cdivision':True, 'language_level':3},
        annotate=True),
    include_dirs=[numpy.get_include()],
    entry_points="""
      [console_scripts]
      fitsddcorrect=jansky.fitsddcorrect:main
      fitstools=jansky.fitstools:main
      casatoaips=jansky.casatoaips:main
      statshist=jansky.statshist:main
      fitsflag=jansky.fitsflag:main
      maskimage=jansky.maskimage:main
      fitshist2d=jansky.fitshist2d:main
      fitshist1d=jansky.fitshist1d:main
      fitsplot=jansky.fitsplot:main
      filtercc=jansky.filtercc:main
      textplot=jansky.textplot:main
      fitsstats=jansky.fitsstats:main
      fitsclip=jansky.fitsclip:main
   """,
)
