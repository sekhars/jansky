"""
Module to do the heavy lifting from fitsclip.all_baselines
"""
import numpy as np
import os
import warnings

from astropy.io import fits
from jansky.lib import fits as jfits
from jansky.lib import const, strings, antennas, stats
from jansky.lib import cstats

from scipy.stats import sigmaclip

def _combined_hist(nbin):
    """
    Reads the binary files per baseline and calculates both the histogram
    per baseline as well as the histogram over all baselines

    Inputs:
        None
    Returns:
        base_hist   Histogram of all baselines, individually 2D numpy array
        base_lims   Limits of the histogram per baseline
        all_hist    Histogram over all baselines, 1D numpy array
        all_lims    Limits of the histogram over all baselines
    """

    all_hist    = np.zeros(nbin+2, dtype=int)
    all_lims    = np.zeros(2)

    strings.clearline()
    # Calculate limits globally and per baseline
    for bb in range(const.NBASELINE):
        if bb % 10 == 0:
            print('Reading baseline {}/{}'.format(bb+1, const.NBASELINE),
                                                                    end="\r")

        filename = 'tmp_base_{:04d}'.format(bb)

        data = np.fromfile(filename, count=-1)

        minval = data.min()
        maxval = data.max()

        if all_lims[0] > minval:
            all_lims[0] = minval
        if all_lims[1] < maxval:
            all_lims[1] = maxval

    for bb in range(const.NBASELINE):
        if bb % 10 == 0:
            print("Calculating histogram over all baselines {}/{}".format(bb+1,
                                                const.NBASELINE), end='\r')
        filename = 'tmp_base_{:04d}'.format(bb)

        data = np.fromfile(filename, count=-1)

        delta = (all_lims[1] - all_lims[0])/nbin

        all_hist += cstats.histogram1d(data, all_lims, delta, nbin)

    return all_hist, all_lims


def _defringe(data, fouriersigma):
    """
    Defringe the input data by eliminating fourier components larger than
    fouriersigma above the mean.

    Inputs:
        data        Input data, 2D

    Returns:
        defr_dat    Defringed data, 2D
    """

    if np.count_nonzero(data) == 0:
        return data

    datfft      = np.fft.fft2(data)
    absfft      = np.abs(datfft)
    clipped_fft = sigmaclip(absfft, fouriersigma, fouriersigma)[0]

    if clipped_fft.size > 0:
        mean = np.mean(clipped_fft)
        std  = np.std(clipped_fft)

        ind = np.where(absfft > (mean + fouriersigma*std))
        datfft[ind] = 0
    else:
        warnings.warn('No clipping done in fourier plane. Defringing failed. '
                'Recommend aborting to avoid excessive flagging of data. '
                'Next time try giving a larger value of --fouriersigma.',
                category=RuntimeWarning)

    idat = np.fft.ifft2(datfft).real

    return idat


def _fits_to_binary(ffile, fitspars, chunk, fouriersigma):
    """
    Writes out each baseline into it's own binary file after defringing.

    Inputs:
        ffile       Astropy FITS file object
        fitspars    Named tuple with FITS file parameters, obtained through the
                    function jansky.lib.fits.fitspars
    Returns:
        None
    """

    for group in range(0, fitspars.gcount, chunk):
        outstr = "Extracting baselines... Group "
        print(outstr, group, "/", fitspars.gcount, end="\r")
        imgdat  = ffile[0].data[group:group+chunk].data
        basepar = ffile[0].data[group:group+chunk].par('BASELINE')

        # If baselines are in AIPS format, change them
        # TODO:This only works because the FITS file is 'complete'. Figure out
        # a more robust check.
        if np.any(basepar[0:10] % 256 > 0):
            basepar = antennas.base_aips_seq(basepar, doself=False)

        for bb in range(const.NBASELINE):
            indices = np.where(basepar == bb+1)
            basedat = np.squeeze(imgdat[indices])

            filename = 'tmp_base_{:04d}'.format(bb)

            if basedat.size <= 0:
                continue

            for pp in range(fitspars.npol):
                datre = np.where(basedat[...,pp,2], basedat[...,pp,0], 0)
                datim = np.where(basedat[...,pp,2], basedat[...,pp,1], 0)

                if datre.size <= fitspars.nchan or datim.size <= fitspars.nchan:
                    continue

                irefft = _defringe(datre, fouriersigma)
                iimfft = _defringe(datim, fouriersigma)

                datlen = datre.size

                # To hold Re/Im per poln and metadata (poln, scan etc.)
                writedat = np.zeros(2*datlen)
                writedat[:datlen] = irefft.ravel()
                writedat[datlen:] = iimfft.ravel()

                with open(filename, 'ab') as fptr:
                    fptr.write(writedat)


def _all_baselines(fitsfile, nsigma, fouriersigma):
    """
    Creates a histogram from all baselines and clips on points above NSIGMA.

    Inputs:
        fitsfile    Name of input FITS file
        nsigma      Sigma above which to clip outliers

    Returns:
        None
    """

    fitspars = jfits.fitspars(fitsfile)
    chunk    = jfits.getfitschunk(fitspars.gcount)

    baselist = [bb + 1 for bb in range(const.NBASELINE)]

    strings.clearline()

    # Write out baselines to binary files
    with fits.open(fitsfile, memmap=True) as ffile:
        _fits_to_binary(ffile, fitspars, chunk, fouriersigma)

    nbin = 1000

    # Histogram over all baselines, hence the all_ prefix.
    all_hist, all_lims  = _combined_hist(nbin)
    all_bins            = stats.get_histbins(all_lims, nbin+2)

    clip_mean, clip_std = stats.hist_stats_clipped(all_hist, all_bins)

    uthresh = clip_mean + nsigma*clip_std
    lthresh = clip_mean - nsigma*clip_std

    ffile = fits.open(fitsfile, memmap=True, mode='update')

    strings.clearline()
    for group in range(0, fitspars.gcount, chunk):
        outstr = "Flagging baselines... Group "
        print(outstr, group, "/", fitspars.gcount, end="\r")
        imgdat  = ffile[0].data[group:group+chunk].data
        basepar = ffile[0].data[group:group+chunk].par('BASELINE')

        # If baselines are in AIPS format, change them
        # TODO:This only works because the FITS file is 'complete'. Figure out
        # a more robust check.
        if np.any(basepar[0:10] % 256 > 0):
            basepar = antennas.base_aips_seq(basepar, doself=False)

        for bb in range(const.NBASELINE):
            indices = np.where(basepar == bb+1)
            basedat = np.squeeze(imgdat[indices])

            if basedat.size <= 0:
                continue

            for pp in range(fitspars.npol):
                datre = np.where(basedat[...,pp,2], basedat[...,pp,0], 0)
                datim = np.where(basedat[...,pp,2], basedat[...,pp,1], 0)

                if datre.size <= fitspars.nchan or datim.size <= fitspars.nchan:
                    continue

                irefft = _defringe(datre, fouriersigma)
                iimfft = _defringe(datim, fouriersigma)

                cond1  = (irefft < lthresh) | (irefft > uthresh)
                cond2  = (iimfft < lthresh) | (iimfft > uthresh)
                cond3  = (datre == 0) | (datim == 0)
                cond   = cond1 | cond2 | cond3

                basedat[...,pp,2] = np.where(cond, 0, basedat[...,pp,2])

            imgdat[indices] = basedat[:,np.newaxis,np.newaxis,np.newaxis,...]

        ffile[0].data[group:group+chunk].base['DATA'] = imgdat

    ffile.close()

    strings.clearline()
    print('Cleaning up', end="\r")
    for bb in range(const.NBASELINE):
        filename = 'tmp_base_{:04d}'.format(bb)
        os.remove(filename)


def _each_stats(nsigma):
    """
    Calculates the sigma clipped stats for each baseline - The stats are
    calculated without creating a histogram, since it is assumed that the
    data will fit in main memory.

    Inputs:
        nsigma  Sigma above which to create the cutoff per baseline

    Returns:
        uthresh List of upper threshold values for clipping
        lthresh List of lower threshold values for clipping
    """

    uthresh = np.zeros(const.NBASELINE)
    lthresh = np.zeros(const.NBASELINE)

    strings.clearline()

    for bb in range(const.NBASELINE):
        if bb % 10 == 0:
            print('Reading baseline {}/{}'.format(bb+1, const.NBASELINE),
                                                                    end="\r")
        filename = 'tmp_base_{:04d}'.format(bb)

        data = np.fromfile(filename, count=-1)

        minval = data.min()
        maxval = data.max()

        if minval == maxval:
            lthresh[bb] = -np.inf
            uthresh[bb] = np.inf
            continue

        __, mean, std, __ = stats.sigmaclipstats(data)

        lthresh[bb] = mean - nsigma*std
        uthresh[bb] = mean + nsigma*std

    return lthresh, uthresh


def _each_baseline(fitsfile, nsigma, fouriersigma):
    """
    Creates a histogram of each baseline and clips independently on each
    baseline. All polarizations and real/imag are treated the same.

    Inputs:
        fitsfile    Name of input FITS file
        nsigma      Sigma above which to clip outliers

    Returns:
        None
    """

    fitspars = jfits.fitspars(fitsfile)
    chunk    = jfits.getfitschunk(fitspars.gcount)

    baselist = [bb + 1 for bb in range(const.NBASELINE)]

    strings.clearline()

    # Write out baselines to binary files
    with fits.open(fitsfile, memmap=True) as ffile:
        _fits_to_binary(ffile, fitspars, chunk, fouriersigma)

    lthresh, uthresh = _each_stats(nsigma)

    ffile = fits.open(fitsfile, memmap=True, mode='update')

    strings.clearline()
    for group in range(0, fitspars.gcount, chunk):
        outstr = "Flagging baselines... Group "
        print(outstr, group, "/", fitspars.gcount, end="\r")
        imgdat  = ffile[0].data[group:group+chunk].data
        basepar = ffile[0].data[group:group+chunk].par('BASELINE')

        # If baselines are in AIPS format, change them
        # TODO:This only works because the FITS file is 'complete'. Figure out
        # a more robust check.
        if np.any(basepar[0:10] % 256 > 0):
            basepar = antennas.base_aips_seq(basepar, doself=False)

        for bb in range(const.NBASELINE):
            indices = np.where(basepar == bb+1)

            basedat = imgdat[indices]

            for pp in range(fitspars.npol):
                datre = np.where(basedat[...,pp,2], basedat[...,pp,0], 0)
                datim = np.where(basedat[...,pp,2], basedat[...,pp,1], 0)

                irefft = _defringe(datre, fouriersigma)
                iimfft = _defringe(datim, fouriersigma)

                cond1  = (irefft < lthresh[bb]) | (irefft > uthresh[bb])
                cond2  = (iimfft < lthresh[bb]) | (iimfft > uthresh[bb])
                cond3  = (datre == 0) | (datim == 0)
                cond   = cond1 | cond2 | cond3

                basedat[...,pp,2] = np.where(cond, 0, basedat[...,pp,2])

            imgdat[indices] = basedat

        ffile[0].data[group:group+chunk].base['DATA'] = imgdat

    ffile.close()

    strings.clearline()
    print('Cleaning up', end="\r")
    for bb in range(const.NBASELINE):
        filename = 'tmp_base_{:04d}'.format(bb)
        os.remove(filename)
