"""
Implementation of Ramana Athreya's windowed statistics function to flag
RFI.
"""

from astropy.io import fits
from jansky.lib import fits as jfits
from jansky.lib import strings
from sys        import stderr

import numpy as np
import click


CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    Run windowed statistics to detect and clip outliers in either the
    time-channel plane (per baseline) or in the UV plane.
    """
    pass

@main.command(short_help="Clip per baseline")
@click.argument('fitsfile', type=click.Path())
@click.option('--uvrange', default=None,
        help='UV Range in lamda within which to clip')
@click.option('--baseline', 'base', type=int, default=None,
        help='Clip only on specified baseline')
@click.option('--sizet', type=int, default=3,
        help='Window half-size in time (in units of integration periods)')
@click.option('--sizec', type=int, default=3,
        help='Window half-size in frequency (in units of channels)')
def baseline(fitsfile, uvrange, base, sizet, sizec):
    """
    Given an input FITSFILE, clips outliers in Re/Im per polarization in the
    time-channel plane (i.e., per baseline).  For a full description,
    refer the docs.
    """

    if uvrange is not None:
        uvrange = strings.minmax(uvrange)

    fitspars    = jfits.fitspars(fitsfile)
    gcount      = fitspars.gcount
    chunk       = jfits.getfitschunk(gcount)

    ffile = fits.open(Fitsfile, memmap=True, mode='update')

    for group in range(0, gcount, chunk):
        print("Processing group ", group, "/", gcount, end="\r", file=stderr)

    ffile.close()
