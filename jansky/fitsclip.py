#! /usr/bin/python

"""
A set of command line tools to run RFI excision on FITS files. Complementary
to fitsflag - This is more automated and careful.
"""

import click

CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    pass

import numpy as np

@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--baseline', '-b', type=int,
            help='Baseline number to clip on  [default: all]')
@click.option('--nsigma', '-n', type=float, default=3.0,
            help='Sigma clipping factor  [default: 3.0]')
@click.option('--fouriersigma', '-f', type=float, default=2.5,
        help='Sigma to clip at in the fourier plane, to remove the fringes '
        'from the data. [default: 2.5]. A more aggressive value will remove '
        'more fringes.')
def baseline_scan(fitsfile, baseline, nsigma, fouriersigma):
    """
    Given an input FITSFILE, will clip points above --nsigma per scan per
    polarization per baseline. This clipping can be specified only for a single
    baseline, or for all.

    Prior to calculating the statistics, the scan is defringed to remove
    statistical biases due to the fringes in the residuals. It is defringed
    by fourier transforming the scan and then throwing away all fourier
    components that are 3 standard deviations above the mean.
    """

    from astropy.io import fits
    from jansky.lib import fits as jfits
    from jansky.lib import const, antennas, stats
    from multiprocessing.pool import Pool
    from jansky.clip.baselines import _defringe

    fitspars = jfits.fitspars(fitsfile)

    group_beg, group_end = jfits.scan_break(fitsfile)

    ffile = fits.open(fitsfile, memmap=True, mode='update')

    if baseline is not None:
        baselist = [baseline,]
    else:
        baselist = [bb+1 for bb in range(const.NBASELINE)]

    gcount = fitspars.gcount

    pool = Pool()
    for  beg, end in zip(group_beg, group_end):
        print("Processing group", beg, "/", gcount, end="\r")

        imgdat  = np.squeeze(ffile[0].data[beg:end+1].data)
        basepar = ffile[0].data[beg:end+1].par('BASELINE')

        if np.any(basepar[0:10] % 256 > 0):
            basepar = antennas.base_aips_seq(basepar, doself=False)

        for bb in baselist:
            for pp in range(fitspars.npol):
                indices = np.where(basepar == bb)
                basedat = imgdat[indices]

                datre   = np.where(basedat[...,pp,2], basedat[...,pp,0], 0)
                datim   = np.where(basedat[...,pp,2], basedat[...,pp,1], 0)

                # Too small to do anything
                if datre.size < 10 or datim.size < 10:
                    continue

                # All zeros
                if np.allclose(datre, 0) and np.allclose(datim, 0):
                    continue

                fsigma = [fouriersigma, fouriersigma]
                args   = zip([datre, datim], fsigma)
                defr = pool.starmap(_defringe, args)

                defre = defr[0]
                defim = defr[1]

                __, mre, sre, __ = stats.sigmaclipstats(defre)
                __, mim, sim, __ = stats.sigmaclipstats(defim)

                lthreshre = mre - nsigma*sre
                lthreshim = mim - nsigma*sim

                uthreshre = mre + nsigma*sre
                uthreshim = mim + nsigma*sim

                cond1 = (defre < lthreshre) | (defre > uthreshre)
                cond2 = (defim < lthreshim) | (defim > uthreshim)
                cond3 = (datre == 0) | (datim == 0)
                cond  = cond1 | cond2 | cond3

                basedat[...,pp,2] = np.where(cond, 0, basedat[...,pp,2])

                imgdat[indices] = basedat

        writedat = imgdat[:, np.newaxis, np.newaxis, np.newaxis, ...]
        ffile[0].data[beg:end+1].base['DATA'] = writedat

    ffile.close()



@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--nsigma', '-n', type=float, default=3.0,
            help='Sigma clipping factor  [default: 3.0]')
@click.option('--fouriersigma', '-f', type=float, default=2.5,
        help='Sigma to clip at in the fourier plane, to remove the fringes '
        'from the data. [default: 2.5]. A more aggressive value will remove '
        'more fringes.')
def all_baselines(fitsfile, nsigma, fouriersigma):
    """
    Given the input FITSFILE, clips points above the NSIGMA cutoff.

    A histogram of all the baselines is created, and the cutoff determined from
    this baseline. All polarizations and the real and imaginary part of the
    signal are treated in the same histogram.

    This will show very clearly any systematics between the polarizations, i.e.,
    if one polarization is constantly higher/lower than another.

    The baselines are defringed prior to calculating the histograms so there
    are no biases from residual signal.

    Only the flags are applied back to the FITS file - The defringing is done
    without touching the original data.
    """

    from jansky.clip.baselines import _all_baselines

    _all_baselines(fitsfile, nsigma, fouriersigma)


@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--nsigma', '-n', type=float, default=3.0,
            help='Sigma clipping factor  [default: 3.0]')
@click.option('--fouriersigma', '-f', type=float, default=2.5,
        help='Sigma to clip at in the fourier plane, to remove the fringes '
        'from the data. [default: 2.5]. A more aggressive value will remove '
        'more fringes.')
def each_baseline(fitsfile, nsigma, fouriersigma):
    """
    Given the inputs FITSFILE, clip points above the NSIGMA cutoff in
    each baseline independently.

    This differs from the all_baseline routine in that it considers the
    statistics for each baseline independently rather than pooling them all
    together.

    No distinction is made between polarizations while calculating the stats,
    to identify and flag out any outliers which are present in one polarization
    but not the other.

    The baselines are defringed prior to calculating the histograms, and the
    flags are merely transferred onto the original un-defringed file. So the
    original file is not touched at all.
    """

    from jansky.clip.baselines import _each_baseline

    _each_baseline(fitsfile, nsigma, fouriersigma)
