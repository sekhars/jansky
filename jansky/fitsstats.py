"""
Calculate statistics in a FITS file
"""

import click
import numpy as np
from numba import jit


CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    Calculate the statistics of a FITS file. Each sub-command does it in a
    different manner, even though some functionality may overlap between the
    subcommands.
    """
    pass


@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--outfile', type=click.File(mode='w'),help='Name of output file')
@click.option('--window', help='Size of sliding window  [default: 5]',default=5)
def sliding_stats(fitsfile, outfile, window):
    """
    Prints out sliding window statistics of the input FITSFILE. Scan boundaries
    are ignored and the window will span them. The mean, median and standard
    deviation are calculated for the sliding window per polarization for
    real and imaginary separately.

    By default the output is stored in FITSFILE.stats but this can be
    overridden by using the --outfile option. If the output file exists it will
    be overwritten.

    The default size of the sliding window is 5x5, but this can be overriden by
    using the --window option.
    """

    from multiprocessing import Pool
    from itertools import repeat
    from astropy.io import fits

    import jansky.lib.fits as jfits
    import pandas as pd

    fitspars    = jfits.fitspars(fitsfile)
    chunk       = jfits.getfitschunk(fitspars.gcount)

    if outfile is None:
        outname = fitsfile + '.stats'
    else:
        outname = outfile

    # Create pool of workers
    pool = Pool()

    meancols = ['MeanReP1', 'MeanImP1', 'MeanReP2', 'MeanImP2']
    medcols = ['MedianReP1', 'MedianImP1', 'MedianReP2', 'MedianImP2']
    stdcols = ['StdReP1', 'StdImP1', 'StdReP2', 'StdImP2']

    with fits.open(fitsfile, memmap=True) as ffile:
        date0 = ffile[0].data[0].par('DATE')
        nchan = fitspars.nchan

        with click.progressbar(length=fitspars.gcount) as bar:
            for group in range(0, fitspars.gcount, chunk):
                date    = ffile[0].data[group:group+chunk].par('DATE')
                imgdat  = np.squeeze(ffile[0].data[group:group+chunk].data)

                datrep1 = np.where(imgdat[...,0,2], imgdat[...,0,0], 0)
                datimp1 = np.where(imgdat[...,0,2], imgdat[...,0,1], 0)
                datrep2 = np.where(imgdat[...,1,2], imgdat[...,1,0], 0)
                datimp2 = np.where(imgdat[...,1,2], imgdat[...,1,1], 0)

                datlist = [datrep1, datimp1, datrep2, datimp2]
                modelist = [0, 1, 2]

                args = zip(datlist, repeat(window), repeat(0))
                statmean = pool.starmap(_statsfilter, args)

                args = zip(datlist, repeat(window), repeat(1))
                statmed = list(pool.starmap(_statsfilter, args))

                args = zip(datlist, repeat(window), repeat(2))
                statstd = list(pool.starmap(_statsfilter, args))

                means = {'meanrep1':statmean[0], 'meanimp1':statmean[1],
                        'meanrep2':statmean[2], 'meanimp2':statmean[3]}
                meds = {'medrep1':statmed[0], 'medimp1':statmed[1],
                        'medrep2':statmed[2], 'medimp2':statmed[3]}
                stds = {'stdrep1':statstd[0], 'stdimp1':statstd[1],
                        'stdrep2':statstd[2], 'stdimp2':statstd[3]}

                mean_df = pd.DataFrame.from_dict(means)
                med_df = pd.DataFrame.from_dict(meds)
                std_df = pd.DataFrame.from_dict(stds)

                df = pd.concat([mean_df, med_df, std_df], axis=1)

                columns = ['meanrep1', 'meanimp1', 'meanrep2', 'meanimp2']
                columns.extend(['medrep1', 'medimp1', 'medrep2', 'medimp2'])
                columns.extend(['stdrep1', 'stdimp1', 'stdrep2', 'stdimp2'])

                if group < chunk:
                    df.to_csv(outname, sep='\t', float_format='% 10.5f',
                    index=False, columns=columns)
                else:
                    df.to_csv(outname, mode='a',sep='\t',float_format='% 10.5f',
                            header=False, index=False, columns=columns)

                if group > 0:
                    bar.update(group)

                del df


@jit(nopython=True, nogil=True, cache=True)
def _statsfilter(array, window, mode=0):
    """
    Applies a median/mean/std filter over the first dimension of the input
    2D array.  This function is specifically to be used by scan_stats.

    The mode keyword is becuase Numba cannot use nopython mode to compare
    strings as yet. The key is as follows:
        0 --> Mean
        1 --> Median
        2 --> Std. Dev

    The output values are stored in out. Must be one dimensional having the
    same shape as array.shape[0]
    """

    size = array.shape[0]

    out = np.zeros(size)

    # No valid points
    if not np.any(array):
        return out

    for ii in range(size):
        if ii < 2*window:
            subarr = array[0:2*window]
        elif ii + window >= size:
            subarr = array[size-2*window:size]
        else:
            subarr = array[ii-window:ii+window]

        if mode == 0:
            out[ii] = np.mean(subarr)
        elif mode == 1:
            out[ii] = np.median(subarr)
        elif mode == 2:
            out[ii] = np.std(subarr)
        else: # No error handling
            pass

    return out


@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--outfile', 'outname', type=click.File(mode='w'),
                                        help='Name of output file')
@click.option('--scan', 'scansplit', default=180,
        help='Duration between scans in seconds [default: 180]')
def scan_stats(fitsfile, outname, scansplit):
    """
    Given an input FITSFILE, will calculate statistics per scan in the FITSFILE
    and output it to OUTFILE.

    By default the name of the output file will just be '.scan_stats' appended
    to the end of the input FITSFILE, but this can be changed by passing the
    --outfile flag.
    """

    import jansky.lib.fits as jfits
    import jansky.lib.const as const

    from astropy.io import fits
    from jansky.lib.stats import consecutive_diff_1d

    from multiprocessing.dummy import Pool as ThreadPool
    from multiprocessing import Pool

    fitspars = jfits.fitspars(fitsfile)
    npol     = fitspars.npol

    if outname is None:
        outname = fitsfile + '.stats'

    outfile = open(outname, mode='w')

    header_mean = "  ".join(['MeanReP{0}  MeanImP{0}'.format(nn+1)
                                                    for nn in range(npol)])
    header_med  = "   ".join(['MedReP{0}   MedImP{0}'.format(nn+1)
                                                    for nn in range(npol)])
    header_std  = "   ".join(['StdReP{0}   StdImP{0}'.format(nn+1)
                                                    for nn in range(npol)])

    header  = '#  ' + header_mean + "  " + header_med + "   " + header_std

    print(header, file=outfile)

    pool = ThreadPool()

    with fits.open(fitsfile, memmap=True) as ffile:
        group = 0
        while group <= fitspars.gcount:
            chunk0 = 100000
            date = ffile[0].data[group:group+chunk0].par('DATE')
            date *= const.SOLARDAY_SEC
            datediff = consecutive_diff_1d(date)

            # A gap of scansplit seconds or more is a scan
            chunk = np.where(datediff >= scansplit)[0]

            if not any(chunk):
                group += chunk0
                continue
            else:
                # np.where returns a tuple of arrays
                # we only want the first element of the first array
                chunk = chunk[0]

            imgdat = np.squeeze(ffile[0].data[group:group+chunk].data)

            datlist = []
            for nn in range(npol):
                datre = np.where(imgdat[...,nn,2], imgdat[...,nn,0], 0)
                datim = np.where(imgdat[...,nn,2], imgdat[...,nn,1], 0)
                datlist.append(datre)
                datlist.append(datim)

            means   = pool.map(np.mean,   datlist)
            medians = pool.map(np.median, datlist)
            stdevs  = pool.map(np.std,    datlist)

            outmeans = "".join('{:10.5f}'.format(el) for el in means)
            outmeds  = "".join('{:10.5f}'.format(el) for el in medians)
            outstds  = "".join('{:10.5f}'.format(el) for el in stdevs)

            print(outmeans, end="", file=outfile)
            print(outmeds, end="", file=outfile)
            print(outstds, end="\n", file=outfile)

            group += chunk + 1
