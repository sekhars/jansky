"""
A collection of small command line tools to do various things to FITS files.
The functions in this file don't really belong anywhere else, so I'm putting
them in here.

Run as fitstools --help for a full description of what it can do, or read
through the click decorators to figure it out.
"""
import click
import jansky.lib.const as const
import numba as nb

from astropy.io import fits
from sys import stderr, exit

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    A collection of miscallaneous tools to use on FITS files. Running just
    'fitstools' will do nothing - One of the subcommands need to be run,
    for example: fitstools ant_to_base 1 2 will output the resulting baseline
    number.

    The individual subcommands helpfiles will provide more information.
    Ex: fitstools ant_to_base --help
    """
    pass


@main.command(short_help='Given ANT1 and ANT2, returns baseline number.')
@click.argument('ant1', type=int)
@click.argument('ant2', type=int)
@click.option(
    '-n',
    '--nantenna',
    type=int,
    default=const.NANTENNA,
    help='Total number of antennas  [default: const.NANTENNA]')
@click.option(
    '-a',
    '--autocorr',
    is_flag=True,
    help='Use this flag to also account for  self-baselines?')
def ant_to_base(ant1, ant2, nantenna, autocorr):
    """
    Given ANT1 and ANT2, returns the baseline number. Can optionally also
    specify the total number of baselines.
    """

    import jansky.lib.antennas as antennas

    ant1, ant2 = [min(ant1, ant2), max(ant1, ant2)]

    baseline = antennas.ant_to_base(ant1, ant2, nantenna, autocorr)
    click.echo('% d' % baseline)


@main.command(short_help='Given input BASELINE, returns the antennas.')
@click.argument('baseline', type=int)
@click.option(
    '-n',
    '--nantenna',
    type=int,
    default=const.NANTENNA,
    help='Total number of antennas  [default: const.NANTENNA]')
@click.option(
    '-a',
    '--autocorr',
    is_flag=True,
    help='Use this flag to also account for self-baselines')
def base_to_ant(baseline, nantenna, autocorr):
    """
    Given an input BASELINE, returns the component antennas. Can optionally
    specify the total number of antennas, whether to count self-baselines or
    not.

    The returned antennas are in the range [1...N]
    """

    import jansky.lib.antennas as antennas

    if baseline < 1 or baseline > nantenna * (nantenna + 1) / 2.:
        lim = nantenna * (nantenna + 1) / 2.
        errmsg = 'Baseline should be with range 1 < baseline < %d' % lim
        raise ValueError(errmsg)

    ant1, ant2 = antennas.base_to_ant(baseline, nantenna, autocorr)


@main.command(short_help='Duplicate POL1 and copy into POL2.')
@click.argument('fitsname', type=click.Path(exists=True))
@click.argument('pol1', type=int)
@click.argument('pol2', type=int)
def duplicate_poln(fitsname, pol1, pol2):
    """
    Duplicate POL1 and copy it into POL2 for the FITS file FITSNAME.
    """
    import jansky.lib.strings as strings
    import jansky.lib.fits as jfits

    fitspars = jfits.fitspars(fitsname)
    npol = fitspars.npol
    gcount = fitspars.gcount

    if (pol1 < 1 or pol1 > npol) or (pol2 < 1 or pol2 > npol):
        errmsg = "Both polarizations must be between 1 and %d" % npol
        raise ValueError(errmsg)

    pol1 = pol1 - 1
    pol2 = pol2 - 1

    ffile = fits.open(fitsname, memmap=True, mode='update')

    chunksize = jfits.getfitschunk(gcount)
    strings.clearline()
    for group in range(0, gcount, chunksize):
        print("Processing group ", group, "/", gcount, end='\r')
        imgdat = ffile[0].data[group:group + chunksize].data
        imgdat[..., pol2, :] = imgdat[..., pol1, :]

        ffile[0].data[group:group + chunksize].base['DATA'] = imgdat
        ffile.flush()

    strings.clearline()
    ffile.close()


@main.command(short_help='Flag an entire polarization of a FITS file')
@click.argument('fitsname', type=click.Path(exists=True))
@click.argument('pol', type=int)
@click.option(
    '--zero',
    default=False,
    is_flag=True,
    help='Zero out the visibility values as well')
def flag_poln(fitsname, pol, zero):
    """
    Flag an entire polarization POL in the FITS file FITSNAME. The polarization
    is specified as a number, i.e., specifiying "1" will flag the first
    polarization etc. If the specified polarization doesn't exist, the program
    will complain and exit.
    """

    import jansky.lib.strings as strings
    import jansky.lib.fits as jfits

    fitspars = jfits.fitspars(fitsname)

    if pol > fitspars.npol:
        errstr = 'Specified polarization {} greater than number '.format(pol)
        errstr += 'of polarizations {}'.format(fitspars.npol)
        raise ValueError(errstr)

    pol = pol - 1  # poln is zero indexed in FITS file

    ffile = fits.open(fitsname, memmap=True, mode='update')

    chunk = jfits.getfitschunk(fitspars.gcount)
    strings.clearline()

    for group in range(0, fitspars.gcount, chunk):
        print('Processing group', group, "/", fitspars.gcount, end="\r")
        imgdat = ffile[0].data[group:group + chunk].data

        imgdat[..., pol, 2] = 0

        if zero:
            imgdat[..., pol, 0] = 0
            imgdat[..., pol, 1] = 0

        ffile[0].data[group:group + chunk].base['DATA'] = imgdat
        ffile.flush()

    strings.clearline()
    ffile.close()


@main.command(short_help='Check if FITS files are complete')
@click.argument('fitsname', type=click.Path(exists=True))
@click.option(
    '--nbase',
    '-n',
    type=int,
    default=const.NBASELINE,
    help='''
      Number of baseline in array. Default is given by lib.const.NBASELINE
      ''')
def check_complete(fitsname, nbase=const.NBASELINE):
    """
    Check if the FITS file is complete
    """

    import jansky.lib.antennas as antennas
    import jansky.lib.fits as jfits
    import numpy as np

    fitspars = jfits.fitspars(fitsname)
    ffile = fits.open(fitsname)

    gcount = fitspars.gcount
    chunksize = jfits.getfitschunk(gcount)

    base0 = ffile[0].data[0].par('BASELINE')
    baselines = ffile[0].data[::nbase].par('BASELINE')
    baselines = antennas.base_aips_seq(baselines)

    basediff = np.ediff1d(baselines)
    count = np.count_nonzero(basediff != 0)

    # TODO : The code below doesn't work so well - The difference between
    # the penultimate and last timestamps is different from the difference
    # between all the other successive timestamps. So FITS files are showing
    # as incomplete even though they are complete. For now the logic of this
    # function mirrors the logic of the C function, but having a timestamp
    # check would be more robust.
    """
    times       = ffile[0].data[::nbase].par('DATE')
    timediff    = np.ediff1d(times)
    timecheck   = np.all(timediff == timediff[0])
   """
    if count == 0:
        print("FITS file is complete")
    else:
        print("FITS file is not complete.")
        exit()

    ffile.close()


@nb.jit(nopython=True, nogil=True, cache=True)
def count_flagged_rows(imgdat, shape):
    """
    Helper function for perc_flagged to count the number of fully flagged rows.
    """

    nflag = 0
    nflagrow = 0

    for nn in range(shape[0]):
        nflag = 0
        for ii in range(shape[1]):
            for jj in range(shape[2]):
                if imgdat[nn, ii, jj, 2] == 0:
                    nflag += 1

        if nflag == shape[1] * shape[2]:
            nflagrow += 1

    return nflagrow


@main.command(short_help='Check percentage of flagged data')
@click.argument('fitsname', type=click.Path(exists=True))
@click.option(
    '--flagged-rows',
    'frows',
    is_flag=True,
    help='Print the number of completely flagged rows')
@click.option(
    '--uvrange',
    nargs=2,
    type=float,
    help='Count percentage of flagged data within given UV range in lambda.')
def perc_flagged(fitsname, frows, uvrange):
    """
    Given a FITS file FITSNAME, checks the percentage of flagged data and
    prints number of flagged & valid visibilities.
    """

    import jansky.lib.strings as strings
    import jansky.lib.fits as jfits
    import numpy as np

    fitspars = jfits.fitspars(fitsname)
    refreq = jfits.getref_freq(fitsname)[0]

    ffile = fits.open(fitsname, memmap=True)

    gcount = fitspars.gcount
    chunksize = jfits.getfitschunk(gcount)

    nflag = 0
    nuflag = 0
    nflagrows = 0

    uscale = ffile[0].header['PSCAL1']
    vscale = ffile[0].header['PSCAL2']
    for group in range(0, gcount, chunksize):
        print("Reading group", group, "/", gcount, end="\r", file=stderr)
        imgdat = np.squeeze(ffile[0].data[group:group + chunksize].data)

        if len(uvrange):
            uvrange = np.asarray(uvrange)
            udat = ffile[0].data[group:group+chunksize].par(0)/uscale
            vdat = ffile[0].data[group:group+chunksize].par(0)/vscale

            uvlen = np.sqrt(udat**2 + vdat**2)
            if uvlen.max() < 1:
                uvlen *= refreq # From lightseconds to lambda

            idx = np.where((uvlen > uvrange.min()) & (uvlen < uvrange.max()))

            imgdat = imgdat[idx]

        if frows:
            nflagrows = count_flagged_rows(imgdat.newbyteorder().byteswap(),
                                           np.array(imgdat.shape))

        nuflag += np.count_nonzero(imgdat[:, :, :, 2] > 0)
        nflag += np.count_nonzero(imgdat[:, :, :, 2] <= 0)

    percflag = nflag / float(nflag + nuflag) * 100.

    strings.clearline()

    print("Total groups:                    % 9d" % gcount)
    print("Number of flagged points:        % 9d" % nflag)
    print("Number of unflagged points:      % 9d" % nuflag)
    print("Percentage of flagged data:     % 10.3f" % percflag)
    if frows:
        print("Number of flagged rows:      % 9d" % nflagrows)


@main.command(short_help='Clip a FITS image between two values')
@click.argument('fitsimage', type=click.Path(exists=True))
@click.option(
    '--min',
    'minval',
    type=float,
    default=None,
    help='Minimum value below which to clip points in the image')
@click.option(
    '--max',
    'maxval',
    type=float,
    default=None,
    help='Maximum value above which to clip points in the image')
def clip_image(fitsimage, minval, maxval):
    """
    Given an input FITSIMAGE, clip points that fall outside the range
    MINVAL < FITSIMAGE < MAXVAL. If minimum or maximum values are not specified
    then the value is assumed to be the minimum//maximum value found in the
    image.

    If the image contains NaN's they are ignored.
    """

    import numpy as np

    ffile = fits.open(fitsimage, memmap=True, mode='update')
    imgdat = np.squeeze(ffile[0].data)

    if minval is None and maxval is None:
        print("No minnimum or maximum value defined. Exiting.")
        exit(1)

    if minval is None:
        minval = np.nanmin(imgdat)
    if maxval is None:
        maxval = np.nanmax(imgdat)

    imgdat = np.where((imgdat > minval) & (imgdat < maxval), imgdat, 0)

    ffile[0].data[..., :, :] = imgdat
    ffile.close()


@main.command()
@click.argument('fitsfile', type=click.Path(exists=True))
def fits_info(fitsfile):
    """
    Given an input FITSFILE, print the "vital statistics" of the FITS file.
    Namely:
        Number of groups
        Number of channels
        Frequency of first channel
        Channel width
        Integration time
        UV range spanned
    """

    import jansky.lib.fits as jfits
    import numpy as np

    fitspars = jfits.fitspars(fitsfile)
    refreq, refdelt = jfits.getref_freq(fitsfile)
    integration = jfits.fitsintegration(fitsfile)

    gcount = fitspars.gcount

    uvrange = [np.inf, -np.inf]
    with fits.open(fitsfile, memmap=True) as ffile:
        uscale = ffile[0].header['PSCAL1']
        vscale = ffile[0].header['PSCAL2']
        for group in range(0, gcount, 100000):
            ulen = ffile[0].data[group:group + gcount].par(0) / uscale
            vlen = ffile[0].data[group:group + gcount].par(1) / vscale
            uvlen = np.sqrt(ulen**2 + vlen**2)

            if uvrange[0] > uvlen.min():
                uvrange[0] = uvlen.min()

            if uvrange[1] < uvlen.max():
                uvrange[1] = uvlen.max()

    print("Number of groups: % 10d" % fitspars.gcount)
    print("Number of channels: % 10d" % fitspars.nchan)
    print("Reference frequency (first channel) % 10.5f MHz" % (refreq / 1E6))
    print("Number of polarizations % 4d" % fitspars.npol)
    print("Channel width % 15.8f MHz" % (refdelt / 1E6))
    print("Integration time:")
    print("              Mean: % 15.8f s" % integration[0])
    print("            Median: % 15.8f s" % integration[2])
    print("Standard Deviation: % 15.8f s" % integration[1])
    print("UV Range % d - % d" % (uvrange[0], uvrange[1]))


@main.command(short_help='Perform basic arithmetic on two FITS files.')
@click.argument('fits1', type=click.Path(exists=True))
@click.argument('operator')
@click.argument('fits2', type=click.Path(exists=True))
def fits_math(fits1, fits2, operator):
    """
    Perform the operation defined by OPERATOR on FITS1 and FITS2. The results
    of the operation are stored in FITS1.

    Currently supported operators are '+', '-', '*'. '/'. Make sure to
    specify the operator within quotes, otherwise the shell will expand '*' to
    mean all the files in the directory.
    """

    import jansky.lib.fits as jfits

    inp_op = operator  # This is the input from command line. Not the module

    import operator

    ops_dict = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv
    }

    op = ops_dict[inp_op]

    fitspars1 = jfits.fitspars(fits1)
    fitspars2 = jfits.fitspars(fits2)

    if fitspars1 != fitspars2:
        raise ValueError('The input FITS files do not have the same structure')

    chunk = jfits.getfitschunk(fitspars1.gcount)

    ffile1 = fits.open(fits1, memmap=True, mode='update')
    ffile2 = fits.open(fits2, memmap=True)

    for group in range(0, fitspars1.gcount, chunk):
        print("Processing group", group, "/", fitspars1.gcount, end="\r")

        dat1 = ffile1[0].data[group:group + chunk].data
        dat2 = ffile2[0].data[group:group + chunk].data

        ffile1[0].data[group:group + chunk].base['DATA'] = op(dat1, dat2)

    ffile1.close()
    ffile2.close()


@main.command(short_help='Set the entire FITSFILE to a single value.')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--value',
    '-v',
    default=1E-5,
    help='Replace all values with this value.  [default: 1E-9]')
@click.option(
    '--keep-flags',
    '-k',
    'dokeep',
    is_flag=True,
    help='Keep the flags in the FITS file.')
def fits_reset(fitsfile, value, dokeep):
    """
    Resets all the values of an input visibility FITSFILE to the value
    specified by --value. By default it is 1E-5.

    All flagged points will also be unflagged, unless --keep-flags is passed.
    """

    import jansky.lib.fits as jfits

    fitspars = jfits.fitspars(fitsfile)
    chunk = jfits.getfitschunk(fitspars.gcount)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:
        for group in range(0, fitspars.gcount, chunk):
            print("Processing group", group, "/", fitspars.gcount, end="\r")

            imgdat = ffile[0].data[group:group + chunk].data

            # Unflag all the data
            if not dokeep:
                imgdat[..., 2] = 1

            imgdat[..., 0] = value
            imgdat[..., 1] = value

            ffile[0].data[group:group + chunk].base['DATA'] = imgdat
            ffile.flush()


@main.command(short_help='Reset all the weights in the FITS file')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--reverse',
    '-r',
    is_flag=True,
    help='Reverse the operation, i.e., flag all the points')
def reset_weights(fitsfile, reverse):
    """
    Resets all the flags in the input visibility FITSFILE, i.e., sets all the
    data weights to 1 indicating the data is un-flagged.

    The --reverse option will instead set all the weights to zero, indicating
    all the data is flagged.
    """

    import jansky.lib.fits as jfits

    fitspars = jfits.fitspars(fitsfile)
    chunk = jfits.getfitschunk(fitspars.gcount)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:
        for group in range(0, fitspars.gcount, chunk):
            print("Processing group", group, "/", fitspars.gcount, end="\r")

            imgdat = ffile[0].data[group:group + chunk].data

            # Unflag all the data
            if reverse:
                imgdat[..., 2] = 0
            else:
                imgdat[..., 2] = 1

            ffile[0].data[group:group + chunk].base['DATA'] = imgdat
            ffile.flush()


@main.command(short_help="Removes the DC component per scan per baseline")
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--nantenna',
    default=const.NANTENNA,
    help='Number of antennas in array  [default: 30]')
@click.option('--uvrange', help='UV range within which to run [default: all]')
def zero_dc_scan(fitsfile, nantenna, uvrange):
    """
    Given an input FITSFILE, removes the DC component on a scan-by-scan basis
    per baseline. The DC component is identified as the zero-frequency bin
    in an FFT.
    """

    import numpy as np
    from jansky.lib import fits as jfits
    from jansky.lib import antennas, strings

    fitspars = jfits.fitspars(fitsfile)

    group_beg, group_end = jfits.scan_break(fitsfile)

    nbaseline = (nantenna * (nantenna - 1)) // 2

    if uvrange is not None:
        uvrange = strings.minmax(uvrange)
    else:
        uvrange = [-np.inf, np.inf]

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:

        uscale = ffile[0].header['PSCAL1']
        vscale = ffile[0].header['PSCAL2']

        for beg, end in zip(group_beg, group_end):
            print(
                "Processing group {}/{}".format(beg, fitspars.gcount),
                end="\r")

            basepar = ffile[0].data[beg:end + 1].par('BASELINE')
            uval = ffile[0].data[beg:end + 1].par(0) / uscale
            vval = ffile[0].data[beg:end + 1].par(1) / vscale
            uvlen = np.sqrt(uval**2 + vval**2)

            # If baselines are in AIPS format, change them
            # TODO:This only works because the FITS file is 'complete'. Figure
            # out a more robust check.
            if np.any(basepar[0:10] % 256 > 0):
                basepar = antennas.base_aips_seq(basepar, nantenna, False)

            imgdat = np.squeeze(ffile[0].data[beg:end + 1].data)

            for bb in range(nbaseline):
                cond1 = basepar == bb + 1
                cond2 = uvlen > uvrange.min
                cond3 = uvlen < uvrange.max
                cond = cond1 & cond2 & cond3

                if np.count_nonzero(cond) == 0:
                    continue

                indices = np.where(cond)
                basedat = imgdat[indices]

                for pp in range(fitspars.npol):
                    datre = np.where(basedat[..., pp, 2], basedat[..., pp, 0],
                                     0)
                    datim = np.where(basedat[..., pp, 2], basedat[..., pp, 1],
                                     0)

                    fftre = np.fft.fft2(datre)
                    fftim = np.fft.fft2(datim)
                    fftre[0] = fftim[0] = 0

                    datre = np.fft.ifft2(fftre).real
                    datim = np.fft.ifft2(fftim).real

                    basedat[..., pp, 0] = np.where(datre != 0, datre,
                                                   basedat[..., pp, 0])
                    basedat[..., pp, 1] = np.where(datre != 0, datre,
                                                   basedat[..., pp, 1])

                imgdat[indices] = basedat

            ffile[0].data[beg:end + 1].base['DATA'] = imgdat[:, None, None,
                                                             None, ...]

            ffile.flush()


@main.command(short_help="Creates an RMS map from a FITS image")
@click.argument('fitsimage', type=click.Path(exists=True))
@click.argument('window', type=int)
@click.argument('step', type=int)
def rms_image(fitsimage, window, step):
    """
    Given an input FITS image, calculates the RMS map and writes it out to
    a new FITS file.

    For a given pixel, the RMS is calculated using the NxN pixels around it,
    where N is given by WINDOW. The RMS is calculated every STEP pixels.
    Cubic interpolation is then performed to estimate the RMS over the
    whole map.
    """

    import os
    import numpy as np
    from shutil import copy2
    from jansky.lib import cstats, strings, stats
    from scipy.ndimage.interpolation import zoom

    dirname, fitsname = os.path.split(fitsimage)
    rmsname = fitsname.upper().split('.FITS')[0] + '_RMS.FITS'
    rmspath = os.path.join(dirname, rmsname)

    if not os.path.exists(rmspath):
        copy2(fitsimage, rmspath)

    with fits.open(fitsimage, memmap=True) as ffile:
        imgdat = np.squeeze(ffile[0].data)

    imgdat = imgdat.byteswap().newbyteorder()

    print("Calculating RMS", end="\r")
    rmsimage = stats.rolling_std(imgdat, window, step, do_sigmaclip=True)

    nonzero_indices = np.where(rmsimage != 0)
    xvals = np.sort(np.unique(nonzero_indices[0]))
    yvals = np.sort(np.unique(nonzero_indices[1]))

    shrunk_image = rmsimage[nonzero_indices].reshape(xvals.size, yvals.size)

    fact = imgdat.shape[0] / shrunk_image.shape[0]

    strings.clearline()
    print("Interpolating image", end="\r")
    zoom_image = zoom(shrunk_image, fact)

    rmsimage = zoom_image.byteswap().newbyteorder()

    strings.clearline()
    print("Writing image", end="\r")
    with fits.open(rmspath, mode='update', memmap=True) as ffile:
        ffile[0].data = rmsimage


@main.command(short_help="Inverts flags in a UV FITS file")
@click.argument('fitsfile', type=click.Path(exists=True))
def invert_flags(fitsfile):
    """
    Given an input UV FITS file, inverts all the flags. Useful for imaging
    only the flagged data. This will however overwrite any weights that may
    have been assigned to the visibilities during the imaging or calibration
    process. The new weights will be either 0 or 1.
    """

    import numpy as np
    import jansky.lib.fits as jfits

    fitspars = jfits.fitspars(fitsfile)
    gcount = fitspars.gcount
    chunk = jfits.getfitschunk(gcount)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:
        for group in range(0, gcount, chunk):
            print("Processing group {}/{}".format(group, gcount), end="\r")

            imgdat = ffile[0].data[group:group + chunk].data
            imgdat[..., 2] = np.invert(imgdat[..., 2].astype(int))

            ffile[0].data[group:group + chunk].base['DATA'] = imgdat


@main.command(short_help="Matches weights between two FITS files")
@click.argument('fitsmatchee', type=click.Path(exists=True))
@click.argument('fitsmatcher', type=click.Path(exists=True))
def match_weights(fitsmatchee, fitsmatcher):
    """
    Given two input UV FITS files matches the weights from FITSMATCHEE to
    the weights in FITSMATCHER, that is it copies the weights from FITSMATCHER
    to the weights in FITSMATCHEE.

    The structures of the two FITS files must be identical.
    """

    import numpy as np
    import jansky.lib.fits as jfits

    fitspars1 = jfits.fitspars(fitsmatcher)
    fitspars2 = jfits.fitspars(fitsmatchee)

    for index, (ff1, ff2) in enumerate(zip(fitspars1, fitspars2)):
        if ff1 != ff2:
            print(fitspars1[index], fitspars2[index])
            print(fitspars1, fitspars2)
            print("Structures of the FITS files do not match. Exiting.")
            exit()

    gcount = fitspars1.gcount
    chunk = jfits.getfitschunk(gcount)

    fmatcher = fits.open(fitsmatcher, memmap=True)
    fmatchee = fits.open(fitsmatchee, memmap=True, mode='update')

    for group in range(0, gcount, chunk):
        print("Processing group {}/{}".format(group, gcount), end="\r")

        imgdat1 = fmatcher[0].data[group:group + chunk].data
        imgdat2 = fmatchee[0].data[group:group + chunk].data

        imgdat2[..., 2] = imgdat1[..., 2]
        fmatchee[0].data[group:group + chunk].base['DATA'] = imgdat2

    fmatcher.close()
    fmatchee.close()


@main.command(short_help="Matches weights between two FITS files")
@click.argument('fitsmatchee', type=click.Path(exists=True))
@click.argument('fitsmatcher', type=click.Path(exists=True))
def match_flags(fitsmatchee, fitsmatcher):
    """
    Given two input UV FITS files matches the flags from FITSMATCHEE to
    the weights in FITSMATCHER, that is it copies the flags (i.e., weight=0)
    from FITSMATCHER to the weights in FITSMATCHEE, preserving already flagged
    points in FITSMATCHEE.

    The structures of the two FITS files must be identical.
    """

    import numpy as np
    import jansky.lib.fits as jfits

    fitspars1 = jfits.fitspars(fitsmatcher)
    fitspars2 = jfits.fitspars(fitsmatchee)

    for index, (ff1, ff2) in enumerate(zip(fitspars1, fitspars2)):
        if not np.allclose(ff1, ff2):
            print(fitspars1[index], fitspars2[index])
            print(fitspars1, fitspars2)
            print("Structures of the FITS files do not match. Exiting.")
            exit()

    gcount = fitspars1.gcount
    chunk = jfits.getfitschunk(gcount)

    fmatcher = fits.open(fitsmatcher, memmap=True)
    fmatchee = fits.open(fitsmatchee, memmap=True, mode='update')

    for group in range(0, gcount, chunk):
        print("Processing group {}/{}".format(group, gcount), end="\r")

        imgdat1 = fmatcher[0].data[group:group + chunk].data
        imgdat2 = fmatchee[0].data[group:group + chunk].data

        cond1 = imgdat1[..., 2] == 0
        cond2 = imgdat2[..., 2] == 0
        cond = cond1 | cond2

        imgdat2[..., 2] = np.where(cond, 0, imgdat2[..., 2])
        # Set associated visibilities to zero as well
        imgdat2[..., 0] = np.where(cond, 0, imgdat2[..., 0])
        imgdat2[..., 1] = np.where(cond, 0, imgdat2[..., 1])

        fmatchee[0].data[group:group + chunk].base['DATA'] = imgdat2

    fmatcher.close()
    fmatchee.close()


@main.command(short_help="Masks pixels below sigma threshold")
@click.argument('fitsimage', type=click.Path(exists=True))
@click.option(
    '--nsigma',
    type=float,
    default=3,
    help='Mask pixels below NSIGMAxMEDIAN  [default:3]')
@click.option(
    '--use-mean',
    'use_mean',
    is_flag=True,
    help='Use mean instead of the median')
@click.option(
    '--box',
    type=int,
    default=10,
    help='Half size of the box to use to calculate'
    'the standard deviation in pixels  [default:5]')
@click.option('--sigmaclip', is_flag=True, help='Use sigma clipped statistics')
def mask_image(fitsimage, nsigma, use_mean, box, sigmaclip):
    """
    Given an input FITS image, masks pixels below the given sigma threshold.
    """

    import numpy as np
    import jansky.lib.fits as jfits
    from jansky.lib import stats
    from scipy.signal import medfilt
    from scipy.ndimage.filters import generic_filter

    import matplotlib.pyplot as plt

    with fits.open(fitsimage, mode='update') as ffile:
        imgdat = np.squeeze(ffile[0].data)
        imgdat = imgdat.byteswap().newbyteorder()

        stdmap = stats.rolling_std(
            imgdat, window=2 * box + 1, do_sigmaclip=sigmaclip)
        medmap = medfilt(imgdat, 2 * box + 1)

        mask = imgdat < medmap + nsigma * stdmap
        imgdat[mask] = np.nan

        imgdat = imgdat[None, None, :, :]

        ffile[0].data = imgdat


@main.command(short_help="Print the phase centre of the FITS file")
@click.argument('fitsfile', type=click.Path(exists=True))
def phase_centre(fitsfile):
    """
    Given an input FITSFILE, print the phase centre in HMS, DMS formats.
    """

    from astropy.coordinates import SkyCoord

    with fits.open(fitsfile, memmap=True) as ffile:

        for index, key in enumerate(ffile[0].header):
            if "CTYP" in key.upper():
                if "RA" in ffile[0].header[key].upper():
                    num = key[-1]
                    strval = 'CRVAL' + num
                    ra = ffile[0].header[strval]

                if "DEC" in ffile[0].header[key].upper():
                    num = key[-1]
                    strval = 'CRVAL' + num
                    dec = ffile[0].header[strval]

    coord = SkyCoord(ra, dec, unit='degree')
    print(coord.to_string('hmsdms'))


@main.command(short_help="Convert decimal degrees to HMS and DMS")
@click.argument('inpdeg', type=float)
def degtohms(inpdeg):
    """
    Given the input INPDEG in decimal degrees, returns the value in
    hour-minute-second (HMS) and degree-minute-second (DMS) format.
    """

    hour = inpdeg / (360. / 24.)
    rem = abs(hour - int(hour))
    hour = int(hour)

    minute = rem * 60
    rem = rem * 60 - int(minute)
    minute = int(minute)

    sec = rem * 60

    print("HMS: {:d}h{:d}m{:05.2f}s".format(hour, minute, sec))

    deg = int(inpdeg)
    rem = abs(inpdeg - deg)

    minute = rem * 60
    rem = rem * 60 - int(minute)
    minute = int(minute)

    sec = rem * 60

    print("DMS: {:d}d{:d}m{:05.2f}s".format(deg, minute, sec))
