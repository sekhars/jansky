#! /usr/bin/env python3

"""
Plotting routines for FITS files - This should ideally be a wrapper
around all the various plotting routines that evolve with time.

Histogram plotting, bandpass plotting etc.

The functions here are wrappers around other functions that can span entire
modules. The reason these wrapper functions exist is to collect all the
functionality in one place for easy reference and maintainence.
"""

import click
import jansky.plot


CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    A set of command line tools to plot FITS files in various ways. No
    flagging/clipping is done in these routines, they are meant purely for
    visualization.
    """
    pass


@main.command(short_help='Plot the bandpass shape of an input LTA file')
@click.argument('inpfile', type=click.Path(exists=True))
def bandpass(inpfile):
    """
    Plots the bandpass of the input file INPFILE. The bandpass is plotted for
    the first timestamp in the file. The file can either be a FITS file or
    an LTA file - The type is determined by the filename extension.
    """

    jansky.plot.bandpass(inpfile)

@main.command(short_help='EXPERIMENTAL: Plot the UV grid of a FITS file.')
@click.argument('fitsfile', type=click.Path(exists=True))
def uvgrid(fitsfile):
    """
    Displays the UV grid at a precision of 1 lambda.
    """

    from astropy.io         import fits

    import pandas           as pd
    import jansky.lib.fits  as jfits
    import numpy            as np

    import datashader       as ds
    import datashader.transfer_functions as tf

    from datashader.callbacks   import InteractiveImage
    from datashader.colors      import Greys9, viridis, Hot

    from bokeh.io               import show, output_file

    import bokeh.plotting       as bp

    fitspars = jfits.fitspars(fitsfile)
    chunk    = jfits.getfitschunk(fitspars.gcount)
    gcount   = fitspars.gcount
    nchan    = fitspars.nchan

    ffile = fits.open(fitsfile, memmap=True)

    uscale   = ffile[0].header['PSCAL1']
    vscale   = ffile[0].header['PSCAL2']

    freqfactor  = np.array(1. +
            (fitspars.chanwidth * np.arange(nchan)/fitspars.refreq))

    for group in range(0, gcount, chunk):
        print("Reading group ", group, "/", gcount, end="\r")

        uval    = ffile[0].data.par(0)[group:group+chunk]/uscale
        vval    = ffile[0].data.par(1)[group:group+chunk]/vscale

        uval[vval < 0] *= -1
        vval[vval < 0] *= -1

        ushape  = uval.shape
        vshape  = vval.shape

        # Reshape to match image array shape
        uval    = np.repeat(uval, nchan).reshape(ushape[0], nchan)
        vval    = np.repeat(vval, nchan).reshape(vshape[0], nchan)

        # U & V values change with frequency
        uval    = uval * freqfactor
        vval    = vval * freqfactor

        imgdat  = np.squeeze(ffile[0].data[group:group+chunk].data)
        ampdat1 = np.where(imgdat[...,0, 2],
                            np.hypot(imgdat[...,0, 0], imgdat[...,0,1]), 0)
        ampdat2 = np.where(imgdat[...,1, 2],
                            np.hypot(imgdat[...,1, 0], imgdat[...,1,1]), 0)

        uval    = uval.flatten()
        vval    = vval.flatten()

        ampdat1 = ampdat1.flatten()
        ampdat2 = ampdat2.flatten()

        recarr  = np.rec.fromarrays((uval, vval, ampdat1, ampdat2),
                dtype=[('uval', 'f8'), ('vval', 'f8'), ('ampp1', 'f8'),
                    ('ampp2', 'f8')])

        if group <= chunk:
            df = pd.DataFrame(recarr)
        else:
            df.append(pd.DataFrame(recarr))

    df.reset_index(drop=True, inplace=True)

    x_range = [df['uval'].min(), df['uval'].max()]
    y_range = [df['vval'].min(), df['vval'].max()]

    # Callback function for datashader
    def create_image(x_range, y_range, width, height):
        cvs = ds.Canvas(x_range=x_range, y_range=y_range, plot_height=height,
                plot_width=width)

        agg = cvs.points(df, 'uval', 'vval', ds.mean('ampp1'))
        img = tf.interpolate(agg, cmap=reversed(Greys9), how='linear')
        img = tf.set_background(img, color='black')

        return img

    img = create_image(x_range, y_range, 11000, 11000)

    tools = 'wheel_zoom,box_zoom,pan,hover,resize,reset'
    plt = bp.figure(plot_width=1000, plot_height=1000, x_range=x_range,
            y_range=y_range, responsive=True, tools=tools)
    InteractiveImage(plt, create_image, throttle=0)

    output_file('tmp.html')
    show(plt)

    ffile.close()



@main.command(short_help='Plot a 1D/2D histogram of a FITS file.')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.argument('xaxis', nargs=1, required=True)
@click.argument('yaxis', nargs=1, required=False)
@click.option('--pol', 'poln', type=click.Choice(['1','2','3']), default='3',
        help='Polarization to plot  [default: 3]')
@click.option('--force1d', is_flag=True,
        help='Force 1 dimensionsl histograms when X and Y axis are '
        'specified. This allows for comparative plots between X and Y')
def hist(fitsfile, xaxis, yaxis, poln, force1d):
    """
    Given an input FITSFILE, plots a histogram of the quantity specified on
    XAXIS. If YAXIS is also specified, plots a two dimensional histogram
    between the quantities specified on the X and Y axes.

    The quantities can be any one of the following:
        * Amplitude
        * Phase
        * Real
        * Imaginary
        * UVLength
        * Time

    The polarization can optionally be specified via the --pol flag,
    either 1, 2 or 3 to plot the first, second or all polarizations
    respectively.
    """

    from jansky.plot import histogram

    try:
        poln = int(poln)
    except ValueError as e:
       errmsg = 'Value for --poln must be an integer'
       raise Exception(errmsg) from e


    histogram(fitsfile, xaxis, yaxis, poln)


@main.group(short_help='Plot one/many baselines in different ways.')
def baseline():
    """
    Each subcommand of the fitsplot baseline command plots baseline data
    in a different manner. Read the descriptions of each one for a more
    detailed explanation of what each command does.
    """
    pass

@baseline.command('time_series',
                        short_help='Plot the time series of a single baseline')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.argument('baseline', type=int)
@click.option('--mode', type=click.Choice(['stacked', 'average']),
        help='Either stack the polarization plots or average the '
        'polarizations before plotting', default='stacked')
@click.option('--average', type=click.Choice(['mean', 'median', 'none']),
        help='Method used to average over channels before plotting',
        default='median')
def _time_series(fitsfile, baseline, mode, average):
    """
    Given an input FITSFILE, plot the time series amplitude of the given
    BASELINE. The input BASELINE should be an integer between 1 and the
    maximum number of baselines.

    By default each polarization will be stacked on the same plot, but
    passing --mode = 'average' will average the polarizations before plotting.

    By default the median over all the channels will be calculated before
    plotting. If --average is 'none', then no averaging will be performed over
    channels. However this may result in misleading plots since the channels
    will appear along the time axis.
    """

    from jansky.plot import baseplot

    baseplot.time_series(fitsfile, baseline, mode, average)


@baseline.command('average',short_help='Plot the average values of all baselines')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option('--average', type=click.Choice(['median', 'mean']),
        help='Use either mean or median to calculate average  [default: median]',
        default='median')
def _average(fitsfile, average='median'):
    """
    Given an input FITSFILE, plots the average amplitude per baseline
    (average over all times and channels) for all baselines.
    Useful to see if any baseline is consistently high. The averages are
    plotted separately for each polarization.
    """

    from jansky.plot import baseplot

    baseplot.average(fitsfile, average)
