#! /usr/bin/python

"""
Usage:
    fitsflag <command> <options>

    fitsflag uvrange FITSFILE  [options]

A set of command line utilities to flag FITS files on various parameters/
slices of the UV data. Run fitsflag <command> --help for a usage and
description of each command.
"""

import click
import numpy                as np
import jansky.lib.fits      as jfits
import jansky.lib.strings   as strings
import jansky.lib.const     as const
import jansky.lib.stats     as stats
import jansky.lib.numeric   as numeric
import jansky.lib.antennas  as ant

from astropy.io             import fits
from sys                    import stderr

CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
     """
     A set of command line tools to flag FITS files. All operations are
     done in-place, i.e., no backup is created before flagging. The options
     for each command can be accessed by using:

     fitsflag <command> --help
     """
     pass

@main.command(short_help="Flag within a UV range")
@click.argument('fitsfile', type=click.Path())
@click.argument('uvrange')
@click.argument('alim', type=float)
@click.option('--urange',  is_flag=True, default=False,
      help='Flag on URANGE rather than UVRANGE')
@click.option('--vrange',  is_flag=True, default=False,
      help='Flag on VRANGE rather than UVRANGE')
def uvrange(fitsfile, uvrange, alim, urange, vrange):
    """
    Given an input FITSFILE flags values above AMPLIM between the
    input UVRANGE. The UV range is specified in lambda as a comma separated
    string (ex; 0,30000 or 500,1500) and AMPLIM is in jansky.

    Can optionally only flag on urange or vrange if specified. This will use
    the value specified by the UVRANGE argument to flag on urange/vrange.

    Negative UVRANGE values should be passed after first passing all the
    options, followed by '--', i.e.,
    fitsflag uvrange --urange 100 -- FITSFILE -3000,3000
    """

    fitspars = jfits.fitspars(fitsfile)
    gcount   = fitspars.gcount
    chunk    = jfits.getfitschunk(gcount)
    # Get the uv limits
    uvlim    = strings.minmax(uvrange)

    if all([urange, vrange]):
        raise ValueError("--urange and --vrange cannot be specified"
                "simultaneously. Please specify one or the other")

    if not any([urange, vrange]):
        uvrange = True
    else:
        uvrange = False

    # Wrapping in try/finally to catch any crashes in between
    try:
        ffile    = fits.open(fitsfile, memmap=True, mode='update')

        uscale   = ffile[0].header['PSCAL1']
        vscale   = ffile[0].header['PSCAL2']

        for group in range(0, gcount, chunk):
            print("Processing group", group, "/", gcount, end="\r", file=stderr)

            imgdat  = ffile[0].data[group:group+chunk].data

            if uvrange:
                uval    = ffile[0].data.par(0)[group:group+chunk]/uscale
                vval    = ffile[0].data.par(1)[group:group+chunk]/vscale
                uvval   = np.hypot(uval, vval)

            if urange:
                uvval    = ffile[0].data.par(0)[group:group+chunk]/uscale

            if vrange:
                uvval    = ffile[0].data.par(1)[group:group+chunk]/vscale

            # Pad uvval to match imgdat shape
            cond1   = (uvval > uvlim.min) & (uvval < uvlim.max)
            cond1   = numeric.matchdims(cond1, imgdat[...,2].shape, end=True)

            amp     = np.hypot(imgdat[...,0], imgdat[...,1])
            cond2   = amp > alim
            cond2   = numeric.matchdims(cond2, imgdat[...,2].shape, end=True)

            # Set weight to zero if above conditions are both true
            imgdat[...,2] = np.where(cond1 & cond2, 0, imgdat[...,2])

            # Write back to FITS File
            ffile[0].data[group:group+chunk].base['DATA'] = imgdat
            ffile.flush()

    finally:
       ffile.close()

    strings.clearline()


@main.command(short_help="Clip between user defined limits")
@click.argument('fitsfile', type=click.Path())
@click.option('--uvrange', default=None,
      help='UV range (in lambda) WITHIN which to clip')
@click.option('--amp', 'amp_lim', default=None,
      help='Clip values OUTSIDE amplitude range (in Jy)')
@click.option('--real', 'real_lim', default=None,
      help='Clip values OUTSIDE range (in Jy)')
@click.option('--imag', 'imag_lim', default=None,
      help='Clip values OUTSIDE range (in Jy)')
@click.option('--phase', 'phase_lim', default=None,
      help='Clip values OUTSIDE phase range (in degrees)')
def clip(fitsfile, uvrange, amp_lim, real_lim, imag_lim, phase_lim):
    """
    Given an input FITSFILE, clip values based on the various input
    options. By default, none of the options are set so if the program is
    run with no options it will just silently exit.
    """

    # No condition specified, so just quit
    if not any([amp_lim, real_lim, imag_lim, phase_lim]):
        return

    fitspars    = jfits.fitspars(fitsfile)
    gcount      = fitspars.gcount
    chunk       = jfits.getfitschunk(gcount)

    ffile       = fits.open(fitsfile, memmap=True, mode='update')

    if amp_lim is not None:
        alim  = strings.minmax(amp_lim)
    if real_lim is not None:
        rlim = strings.minmax(real_lim)
    if imag_lim is not None:
        ilim = strings.minmax(imag_lim)
    if phase_lim is not None:
        phslim  = strings.minmax(phase_lim)
    if uvrange is not None:
        uvlim   = strings.minmax(uvrange)

    uscale = ffile[0].header['PSCAL1']
    vscale = ffile[0].header['PSCAL2']

    for group in range(0, gcount, chunk):
        print("Flagging group", group, "/", gcount, end="\r", file=stderr)

        imgdat  = ffile[0].data[group:group+chunk].data

        if uvrange is not None:
            uval    = ffile[0].data[group:group+chunk].par(0)/uscale
            vval    = ffile[0].data[group:group+chunk].par(1)/vscale
            uvval   = np.hypot(uval, vval)

            conduv  = np.where((uvval > uvlim.min) & (uvval < uvlim.max))
        else:
            conduv  = slice(None, None)

        subdat = imgdat[conduv]

        if amp_lim is not None:
            amp  = np.hypot(subdat[...,0], subdat[...,1])
            cond = (amp > alim.min) & (amp < alim.max)
        if phase_lim is not None:
            phase = np.arctan2(subdat[...,1], subdat[...,0])
            cond  = (phase > phslim.min) & (phase < phslim.max)
        if real_lim is not None:
            cond = (subdat[...,0] > rlim.min) & (subdat[...,0] < rlim.max)
        if imag_lim is not None:
            cond = (subdat[...,1] > ilim.min) & (subdat[...,1] < ilim.max)

        if uvrange is not None:
            subdat[...,2] = np.where(cond, subdat[...,2], 0)
        else:
            subdat[...,2] = np.where(cond, subdat[...,2], 0)

        imgdat[conduv] = subdat

        ffile[0].data[group:group+chunk].base['DATA'] = imgdat

        ffile.flush()

    ffile.close()
    strings.clearline()



@main.command(short_help="Flag an entire baseline")
@click.argument('fitsfile', type=click.Path())
@click.argument('baseline', type=int)
@click.argument('poln', type=int)
def baseline(fitsfile, baseline, poln):
    """
    Given an input FITSFILE, will flag all points of BASELINE
    for the specified POLN (polarization).

    To flag all polarizations of a baseline specify POLN to be 0

    Indexing for both POLN and BASELINE start at 1.
    """

    fitspars    = jfits.fitspars(fitsfile)

    if poln > fitspars.npol:
        errmsg = "The number of polarizations is % d " % fitspars.npol
        errmsg += "the input specified % d" % poln
        raise ValueError(errmsg)

    if baseline > const.NBASELINE:
        errmsg = "The number of baselines is % d " % const.NBASELINE
        errmsg += "the input specified was %s" % baseline
        raise ValueError(errmsg)

    if poln == 0:
        flag_all = True
    else:
        flag_all = False

    gcount      = fitspars.gcount
    chunk       = jfits.getfitschunk(gcount)

    ffile       = fits.open(fitsfile, memmap=True, mode='update')

    for group in range(0, gcount, chunk):
        print("Processing group ", group, "/", gcount, end="\r", file=stderr)

        basedat = ffile[0].data[group:group+chunk].par('BASELINE')
        basedat = ant.base_aips_seq(basedat)

        imgdat  = ffile[0].data[group:group+chunk].data

        cond    = basedat == baseline

        if flag_all is True:
            cond = numeric.matchdims(cond, imgdat[...,2].shape, end=True)
        else:
            cond = numeric.matchdims(cond, imgdat[...,poln,2].shape, end=True)


        if flag_all is True:
            imgdat[...,2] = np.where(cond, 0, imgdat[...,2])
        else:
            imgdat[...,poln,2] = np.where(cond, 0, imgdat[...,poln,2])


        ffile[0].data[group:group+chunk].base['DATA'] = imgdat
        ffile.flush()

    ffile.close()
    strings.clearline()
