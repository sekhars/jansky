#! /usr/bin/env python

"""
Usage:
   casatoaips  <FITS_Name>  [options]

   Options:
   -h --help      Display this help message and exit.


   Author: Srikrishna Sekhar
   Date: 2015-03-30
"""

import click

CNT = dict(help_option_names=['-h', '--help'])
@click.command(context_settings=CNT)
@click.argument('fitsfile', type=click.Path(exists=True))
def main(fitsfile):
    """
    There are small differences between UV FITS files written by CASA and as
    written by AIPS. This is a script that reads the input FITS file (assumed to
    be written by CASA) and makes all the necessary changes.

    The changes are as follows:

    (1) Random group parameters:
          CASA writes the group parameters as U, V, W, Date, Date, Baseline
          AIPS writes the group parameters as U, V, W, Baseline, Date, Date
    (2) UV Data:
          CASA writes the UV data as floating point numbers in units of light
          seconds.
          AIPS writes the UV data set as double precision numbers in units of
          lambda.

    All the changes are made to the CASA FITS file to make it correspond to
    an AIPS FITS file.
    """

    from astropy.io import fits
    from jansky.lib import strings

    import jansky.lib.fits  as jfits
    import numpy            as np

    refreq   = jfits.getref_freq(fitsfile)[0]
    fitspars = jfits.fitspars(fitsfile)

    # Open in update mode to write back to FITS file
    ffile    = fits.open(fitsfile, memmap = True, mode = 'update')

    tmptype1 = ffile[0].header['PTYPE4']
    tmpscal1 = ffile[0].header['PSCAL4']
    tmpzero1 = ffile[0].header['PZERO4']
    tmpcomm1 = ffile[0].header.comments['PTYPE4']

    tmptype2 = ffile[0].header['PTYPE5']
    tmpscal2 = ffile[0].header['PSCAL5']
    tmpzero2 = ffile[0].header['PZERO5']
    tmpcomm2 = ffile[0].header.comments['PTYPE5']

    tmptype3 = ffile[0].header['PTYPE6']
    tmpscal3 = ffile[0].header['PSCAL6']
    tmpzero3 = ffile[0].header['PZERO6']
    tmpcomm3 = ffile[0].header.comments['PTYPE6']

    # Swap around the entries in the header
    ffile[0].header['PTYPE4']  = tmptype3
    ffile[0].header['PSCAL4']  = tmpscal3
    ffile[0].header['PZERO4']  = tmpzero3
    ffile[0].header.comments['PTYPE4'] = tmpcomm3

    ffile[0].header['PTYPE5']  = tmptype1
    ffile[0].header['PSCAL5']  = tmpscal1
    ffile[0].header['PZERO5']  = tmpzero1
    ffile[0].header.comments['PTYPE5']  = tmpcomm1

    ffile[0].header['PTYPE6']  = tmptype2
    ffile[0].header['PSCAL6']  = tmpscal2
    ffile[0].header['PZERO6']  = tmpzero2
    ffile[0].header.comments['PTYPE6']  = tmpcomm2

    ffile.flush()

    # Iterate over FITS file in chunks of this size
    chunk   = jfits.getfitschunk(fitspars.gcount)

    # Now modify UV data, and switch the group parameters around
    for group in range(0, fitspars.gcount, chunk):
        print("Modifying group", group, "/", fitspars.gcount, end="\r")

        tmpu  = np.empty_like(ffile[0].data.par(0)[group:group+chunk])
        tmpv  = np.empty_like(ffile[0].data.par(1)[group:group+chunk])
        tmpw  = np.empty_like(ffile[0].data.par(2)[group:group+chunk])

        tmpu  = refreq * ffile[0].data.par(0)[group:group+chunk]
        tmpv  = refreq * ffile[0].data.par(1)[group:group+chunk]
        tmpw  = refreq * ffile[0].data.par(2)[group:group+chunk]

        np.copyto(ffile[0].data.par(0)[group:group+chunk], tmpu)
        np.copyto(ffile[0].data.par(1)[group:group+chunk], tmpv)
        np.copyto(ffile[0].data.par(2)[group:group+chunk], tmpw)

        # Swap the date axes and the baseline axis around
        tmpd1  = np.empty_like(ffile[0].data.par(3)[group:group+chunk])
        tmpd2  = np.empty_like(ffile[0].data.par(4)[group:group+chunk])

        np.copyto(tmpd1, ffile[0].data.par(3)[group:group+chunk], casting='no')
        np.copyto(tmpd2, ffile[0].data.par(4)[group:group+chunk], casting='no')

        ffile[0].data.par(3)[group:group+chunk] = \
                                 ffile[0].data.par(5)[group:group+chunk]
        np.copyto(ffile[0].data.par(4)[group:group+chunk], tmpd1)
        np.copyto(ffile[0].data.par(5)[group:group+chunk], tmpd2)

        # Write changes to FITS file
        ffile.flush()

    invfreq  = 1./refreq

    # Iterate over the header keywords as well as corresponding values
    for key, val in ffile[0].header.items():
        if isinstance(val, str):
            if 'UU' in val: # Update PSCAL keyword to reflect change in UV data
                newkey = key.replace('TYPE', 'SCAL')
                ffile[0].header[newkey] = invfreq

            if 'VV' in val:
                newkey = key.replace('TYPE', 'SCAL')
                ffile[0].header[newkey] = invfreq

            if 'WW' in val:
                newkey = key.replace('TYPE', 'SCAL')
                ffile[0].header[newkey] = invfreq

    # Write changes to HDU
    strings.clearline()
    ffile.close()
