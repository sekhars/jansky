"""
    Given an input image FITSIMAGE and an input model image FITSMODEL filters
    out clean components in FITSMODEL.

    For full usage read the click decorators, and the documentation in the
    main() function
"""

from astropy.io  import fits

import numpy     as np
import numba     as nb

import click


@nb.jit(nopython=True, nogil=True)
def _shrink_rms(rmsdat, stride):
    """
    Collect all the non-zero elements of the rmsdat array in _calc_rms
    into a smaller, contiguous array.

    This way, standard image resampling routines can be used to run the
    2D bilinear interpolation

    Inputs:
        rmsdat  2D numpy array containing points evaluated at regular
                intervals
    Returns:
        conarr  Contiguous array containing only the non-zero elements
                of the input array
    """

    sizex   = rmsdat.shape[0]//stride
    sizey   = rmsdat.shape[1]//stride

    conarr  = np.zeros((sizex, sizey))

    ind0 = 0
    for xx in range(0, rmsdat.shape[0], stride):
        ind1 = 0
        for yy in range(0, rmsdat.shape[1], stride):
            if rmsdat[xx, yy] != 0:
                conarr[ind0, ind1] = rmsdat[xx, yy]

            ind1 += 1
        ind0 += 1

    return conarr


def _calc_rms(fitsimage, window, stride):
    """
    Given the input FITSIMAGE, calculate the local RMS at every STRIDE
    and interpolate.

    Inputs:
        fitsimage       Name of input FITS image, str
        window          Half-size of window over which to calculate RMS, int
        stride          Stride over which to calculate RMS, int

    Returns:
        rmsimg        Interpolated 2D array of local RMS, NumPy NDArray
    """

    from scipy.ndimage.interpolation import zoom
    import jansky.lib.stats as stats

    fimage  = fits.open(fitsimage, memmap=True)

    imgdat  = np.squeeze(fimage[0].data)
    imgdat  = np.nan_to_num(imgdat)

    # Calculate sliding window standard deviation
    rmsdat  = stats.rolling_std(imgdat, window, stride)
    size    = imgdat.shape[0]/stride

    # Keep only non-zero elements
    conarr  = _shrink_rms(rmsdat, stride)

    zoomx   = rmsdat.shape[0]/conarr.shape[0]
    zoomy   = rmsdat.shape[1]/conarr.shape[1]

    # Interpolate back to original size
    rmsimg = zoom(conarr, (zoomx, zoomy), order=1)

    fimage.close()

    return rmsimg


CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('fitsimage', type=click.Path())
@click.argument('fitsmodel', type=click.Path())
@click.option('--nsigpeak', type=float, default=20,
        help='Cutoff for peak flux of clean component  [default:20]')
@click.option('--nsigflux', type=float, default=10,
        help='Cutoff for integrated flux of clean component  [default:10]')
@click.option('--window', type=int, default=5,
        help='Window half-size to calculate local standard deviation  '
        '[default: 5]')
@click.option('--stride', type=int, default=50,
        help='Interval between which to calculate the local standard '
        'deviation  [default: 50]')
@click.option('--opening', type=int,
        help='Perform morphological opening on the model image? '
        "Opening removes small objects smaller than the window size. This is "
        "useful to remove high single pixels that could be due to strong "
        "sidelobes. Using this option when not running multiscale clean "
        "could cause all the clean components to be removed. A good default "
        "value is ~ 5.")
@click.option('--onlypositive', 'do_positive', is_flag=True,
        help='Select only the positive clean components. This overrides '
        'all other selection options.')
def main(fitsimage, fitsmodel, nsigpeak, nsigflux, window, stride, opening,
        do_positive):
    """
    Given an input image FITSIMAGE and an input model image FITSMODEL, the
    program filters out "bad" clean components from FITSMODEL. FITSMODEL
    is modified in-place.

    Clean components with a peak flux greater than NSIGPEAK of the local
    standard deviation at that point are retained. Similarly, if the individual
    clean components are connected together to form larger islands of CCs this
    island is retained if the total flux is greater than NSIGFLUX.

    Any negative clean components are automatically filtered out.

    The local standard deviation is calculated using a sliding window on the
    FITSIMAGE - By default it will calculate the standard deviation every 50
    points and interpolate across these points to save time.
    """

    from scipy.ndimage import grey_opening
    from scipy.ndimage.measurements import label, find_objects
    from jansky.lib import numeric

    if not do_positive:
        # Calculate the RMS image
        rmsimg      = _calc_rms(fitsimage, window, stride)

        fmodel      = fits.open(fitsmodel, memmap=True, mode='update')
        modelimg    = np.squeeze(fmodel[0].data)
        modelimg    = np.nan_to_num(modelimg)
        # Big-endian to little endian conversion required. Seems to be an
        # issue between reading FITS through AstroPy and passing it to SciPy
        modelimg    = modelimg.astype('<f8')
        modelimg[modelimg < 0] = 0 # Kill all negative components

        modeldat    = np.array(modelimg, copy=True)

        # Zero out points that are less than nsigpeak
        indices     = np.where(modeldat < nsigpeak*rmsimg)
        modeldat[indices] = 0

        structure = np.ones([3,3]) # Find connected components
        label_arr, nlabel = label(modelimg, structure=structure)
        obj_loc = find_objects(label_arr) # Location of the connected components

        # Find total flux in each connected region
        int_flux = [np.sum(modelimg[obj]) for obj in obj_loc]

        imagemask   = np.zeros(modelimg.shape, dtype=bool)

        # Kill regions that are below cutoff
        for flux, obj in zip(int_flux, obj_loc):
            cumlsig = np.sqrt(np.sum(rmsimg[obj]*rmsimg[obj]))

            imagemask[obj] = 1

            if flux < nsigflux*cumlsig:
                imagemask[obj] = 0

        # Add the peaks selected earlier to the mask
        indices = np.where(modeldat)
        imagemask[indices] = 1

        writedat = np.where(imagemask, modelimg, 0)
        writedat[writedat < 0] = 0

        if opening is not None:
            writedat = grey_opening(writedat, opening)

    else:
        fmodel      = fits.open(fitsmodel, memmap=True, mode='update')
        writedat    = np.squeeze(fmodel[0].data)
        writedat[writedat < 0] = 0

    writedat = numeric.matchdims(writedat, fmodel[0].data.shape)

    fmodel[0].data = writedat
    fmodel.close()
