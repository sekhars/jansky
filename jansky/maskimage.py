#! /usr/bin/env python

"""
Usage:
    maskimage <fitsimage>  [options]

    <image>         Name of the input image

   --min <val>    Minimum value for colourbar in the image. By default
                  uses the minimum value in the image
   --max <val>    Maximum value for the colourbar in the image. By default
                  uses the maximum value in the image.
   --mask <val>   Set masked values to val  [default: 0]
    -h --help       Display this help text and exit

    This script reads in an input FITS image, and plots it for 
    interactive masking. The FITS file is modified in place.
"""

from docopt                import docopt
from astropy.io            import fits
from matplotlib.patches    import Rectangle
from matplotlib.widgets    import Button

import matplotlib.pyplot    as plt
import numpy                as np

# TODO: Find a way to select multiple areas at once and flag/unflag using
# only a single command. Will save plenty of time

class Draw():
   """
   Draw on a matplotlib axis and return the co-ordinates of the selected 
   region
   """

   def __init__(self, data):
      self.ax           = plt.gca() # Create an axis
      self.rect         = Rectangle((0,0),1,1, alpha=0.5, color='white')
      self.x0           = None
      self.y0           = None
      self.x1           = None
      self.y1           = None
      self.width        = None
      self.height       = None
      self.blc          = None
      self.do_draw      = False
      self.is_pressed   = False

      self.ax.add_patch(self.rect)

      # Connect matplotlib to callback functions
      self.ax.figure.canvas.mpl_connect('button_press_event', self.on_press)
      self.ax.figure.canvas.mpl_connect('button_release_event', self.on_release)
      self.ax.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)

   def on_press(self, event):
      """
      Grab the coordinates of the initial press
      """
      if self.do_draw is False:
         return

      self.x0           = event.xdata
      self.y0           = event.ydata
      self.is_pressed   = True

   def on_release(self, event):
      """
      Redraw rectangle and get coordinates of position of mouse release
      """
      if self.do_draw is False:
         return

      self.x1           = event.xdata
      self.y1           = event.ydata

      self.is_pressed   = False

      self.draw_rect()

   def on_motion(self, event):
      """
      Update the rectangle as the mouse is pressed and dragged
      """
      if self.do_draw is False or self.is_pressed is False:
         return
      
      self.x1  = event.xdata
      self.y1  = event.ydata

      self.draw_rect()

   def draw_rect(self):
      """
      Update the rectangle
      """
      if self.do_draw is False:
         return
      try:
         self.width             = abs(self.x0 - self.x1)
         self.height            = abs(self.y0 - self.y1)
         self.blc               = (min(self.x0, self.x1), min(self.y0, self.y1))

         self.rect.set_width(self.width)
         self.rect.set_height(self.height)
         self.rect.set_xy(self.blc)
         self.ax.figure.canvas.draw()
      except TypeError:  # Cursor is outside boundary of axis
         pass


class FlagButton():
   """
   Click button to flag data
   """

   def __init__(self, draw):
      self.draw   = draw

   def on_click(self, event):
      blc      = self.draw.blc
      width    = self.draw.width
      height   = self.draw.height

      x0       = blc[0]
      x1       = blc[0] + width

      y0       = blc[1]
      y1       = blc[1] + height

      global maskimg, image
      maskimg[y0:y1, x0:x1] = np.ma.masked
      image.set_data(maskimg)

      self.draw.ax.figure.canvas.draw()

 
class UnflagButton():
   """
   Click button to unflag data
   """

   def __init__(self, draw):
      self.draw   = draw # The draw object

   def on_click(self, event):
      blc      = self.draw.blc
      width    = self.draw.width
      height   = self.draw.height

      x0       = blc[0]
      x1       = blc[0] + width

      y0       = blc[1]
      y1       = blc[1] + height

      global maskimg, image
      maskimg[y0:y1, x0:x1].mask = False
      image.set_data(maskimg)

      self.draw.ax.figure.canvas.draw()

class ToggleButton():
   """
   Button to toggle drawing of rectangle on the data
   """
   # The button itself needs to be encapsulated in the class so we can 
   # modify the label on click.
   def __init__(self, draw, ax, label):
      self.draw   = draw
      self.button = Button(ax, label)
      self.button.on_clicked(self.on_click)

   def on_click(self, event):
      self.draw.do_draw = not self.draw.do_draw

      if self.draw.do_draw is False:
         self.button.label.set_text('Enable Selection')

      if self.draw.do_draw is True:
         self.button.label.set_text('Disable Selection')


def maskimage(fitsimage, maskval, minval, maxval):
   """
   Run an interactive matplotlib session to flag data as required
   """
   ffile       = fits.open(fitsimage, memmap=True, mode='update')
   imgdat      = np.squeeze(ffile[0].data)

   # Mask array, to easily flag and unflag data
   global maskimg
   maskimg        = np.ma.array(imgdat, copy=True)

   # Initialize the plot
   fig, ax     = plt.subplots()
   plt.subplots_adjust(bottom=0.2)

   # Define the axes for the buttons
   axflag         = fig.add_axes([0.7, 0.05, 0.1, 0.075])
   axunflag       = fig.add_axes([0.81, 0.05, 0.1, 0.075])
   axtoggle       = fig.add_axes([0.45, 0.05, 0.2, 0.075])

   plt.sca(ax)

   # Create the rectangle drawing stuff
   draw           = Draw(maskimg)

   if minval is None:
      minval = np.nanmin(maskimg.data)
   if maxval is None:
      maxval = np.nanmax(maskimg.data)

   global image
   # Plot the image
   image          = ax.imshow(maskimg, cmap='hot', vmin=minval, vmax=maxval)

   flagbutton     = FlagButton(draw)
   unflagbutton   = UnflagButton(draw)
   togglebutton   = ToggleButton(draw, axtoggle, 'Enable Selection')

   bflag          = Button(axflag, 'Flag')
   bflag.on_clicked(flagbutton.on_click)

   bunflag        = Button(axunflag, 'Unflag')
   bunflag.on_clicked(unflagbutton.on_click)

   plt.show()

   # Replace masked points with the mask value
   fillimg        = np.ma.filled(maskimg, fill_value=maskval)
   # Write back to FITS file
   ffile[0].data[...,:,:] = fillimg

   ffile.close()

def main():
   args        = docopt(__doc__)
   fitsimage   = args['<fitsimage>']
   minval      = args['--min']
   maxval      = args['--max']
   maskval     = float(args['--mask'])

   if minval is not None:
      minval   = float(minval)

   if maxval is not None:
      maxval   = float(maxval)

   maskimage(fitsimage, maskval, minval, maxval)
