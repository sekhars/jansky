"""
Plot baseline information of a FITS file in various ways
"""

import numba as nb

def time_series(fitsfile, baseline, mode='stacked', average='median'):
    """
    Given an input FITSFILE, plot the time series amplitude of the given
    BASELINE. The input BASELINE should be an integer between 1 and the
    maximum number of baselines.

    By default each polarization will be stacked on the same plot, but
    passing --mode = 'average' will average the polarizations before plotting.

    By default the median over all the channels will be calculated before
    plotting. If --average is 'none', then no averaging will be performed over
    channels. However this may result in misleading plots since the channels
    will appear along the time axis.
    """

    import jansky.lib.const as const
    import jansky.lib.fits as jfits
    import jansky.lib.antennas as antennas
    import jansky.lib.strings as strings

    import matplotlib.pyplot as plt
    import pandas as pd
    import numpy as np

    from astropy.io import fits
    from astropy.time import Time

    fitspars    = jfits.fitspars(fitsfile)
    chunk       = jfits.getfitschunk(fitspars.gcount)

    if baseline < 1:
        raise ValueError("Baseline number must be at least 1")
    if baseline > const.NBASELINE:
        raise ValueError('Baseline number must be at most %s' % const.NBASELINE)

    dflist = []
    with fits.open(fitsfile, memmap=True) as ffile:
        for group in range(0, fitspars.gcount, chunk):
            print("Processing group", group, "/", fitspars.gcount, end="\r")

            basepar = ffile[0].data[group:group+chunk].par('BASELINE')

            # AIPS baseline has to have a value of at least 256
            if basepar.min() > 255:
                basepar = antennas.base_aips_seq(basepar, doself=False)

            indices = np.where(basepar == baseline)

            imgdat = np.squeeze(ffile[0].data[group:group+chunk].data)
            imgdat = imgdat[indices]

            date   = ffile[0].data[group:group+chunk].par('DATE')
            date   = date[indices]
            date   = Time(date, format='jd')
            date   = pd.to_datetime(date.datetime)

            datlist = []
            dtypes = []
            for nn in range(fitspars.npol):
                datre = np.where(imgdat[...,nn,2], imgdat[...,nn,0], 0)
                datim = np.where(imgdat[...,nn,2], imgdat[...,nn,1], 0)
                amp = np.hypot(datre, datim)

                if average == 'median':
                    amp = np.median(amp, axis=-1)
                if average == 'mean':
                    amp = np.median(amp, axis=-1)

                datlist.append(amp.flatten())

                dtypes.append(('Pol{}'.format(nn+1), np.float64))

            recarr = np.rec.fromarrays(datlist, dtype=dtypes)

            df = pd.DataFrame(recarr, dtype=np.float64, index=date)
            dflist.append(df)

    strings.clearline()
    print("Plotting...", end="\r")
    # Concatenate all the dataframes
    df = pd.concat(dflist)

    plt.style.use('ggplot')

    if mode == 'average':
        df = df.mean(axis=1)

    ax = df.plot(title='Baseline %d' % (baseline), linewidth=2, alpha=0.7)

    ax.set_xlabel('Time')
    ax.set_ylabel('Amplitude (Jy)')

    plt.show()
    strings.clearline()


def average(fitsfile, average='median'):
    """
    Given an input FITSFILE, plots the average amplitude per baseline
    (average over all times and channels) for all baselines.
    Useful to see if any baseline is consistently high. The averages are
    plotted separately for each polarization.
    """


    import jansky.lib.fits as jfits
    import jansky.lib.const as const
    import jansky.lib.antennas as antennas
    import jansky.lib.strings as strings

    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt

    from astropy.io import fits

    fitspars = jfits.fitspars(fitsfile)
    chunk = jfits.getfitschunk(fitspars.gcount)

    avgarr = np.zeros([fitspars.npol, const.NBASELINE])

    with fits.open(fitsfile, memmap=True) as ffile:
        for group in range(0, fitspars.gcount, chunk):
            print("Processing group", group, "/", fitspars.gcount, end="\r")
            basepar = ffile[0].data[group:group+chunk].par('BASELINE')

            # AIPS baseline has to have a value of at least 256
            if basepar.min() > 255:
                basepar = antennas.base_aips_seq(basepar, doself=False)

            imgdat = np.squeeze(ffile[0].data[group:group+chunk].data)

            for bb in range(const.NBASELINE):
                for pp in range(fitspars.npol):
                    indices = np.where(basepar == bb+1)
                    basedat = imgdat[indices]
                    basere = np.where(basedat[...,pp,2], basedat[...,pp,0], 0)
                    baseim = np.where(basedat[...,pp,2], basedat[...,pp,1], 0)

                    baseamp = np.hypot(basere, baseim)
                    baseamp = baseamp[baseamp != 0]

                    if baseamp.size == 0:
                        continue

                    if average == 'median':
                        avgarr[pp][bb] = np.median(baseamp)
                    else:
                        avgarr[pp][bb] = np.mean(baseamp)

        strings.clearline()
        print("Plotting...", end="\r")

        plt.style.use('ggplot')

        fig, ax = plt.subplots(1,1)

        for pp in range(fitspars.npol):
            plt.plot(avgarr[pp], label='Pol {}'.format(pp+1))

        ax.set_xlabel('Baseline number')
        ax.set_ylabel('Average amplitude (Jy)')
        plt.legend()
        plt.show()
