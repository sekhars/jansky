"""
Plot the bandpass of the first timestamp - Useful to see the bandshape at a
glance.
"""

def _parse_logfile(logfile):
    """
    Parse the logfile created by listscan and modify parameters.

    Inputs:
        logfile     Name of the log file created by listscan

    Returns:
        None
    """
    import re

    swapself = re.compile('^SELF\s+0')
    findscan = re.compile('^Scan', re.IGNORECASE)

    with open(logfile, "r") as fptr:
        loglines = fptr.readlines()

    scanind = None

    # Now replace a few lines in inpfile.log and run gvfits
    for index, line in enumerate(loglines):
        if 'TEST.FITS' in line:
            loglines[index] = line.replace('TEST.FITS', 'BSHAPE.FITS')

        if swapself.match(line):
            replace = swapself.sub('SELF           1', line)
            loglines[index] = replace

        if 'C07' in line or 'S05' in line:
            line = line.strip('S05')
            line = line.strip()
            line = line.strip('C07')
            line = line.strip()

            loglines[index] = line

        # Only use the first scan - Rest are redundant
        if scanind is None:
            if findscan.match(line):
                scanind = index

    with open(logfile, 'w') as fptr:
        fptr.writelines(loglines[0:scanind+1])


def bandpass(inpfile):
    """
    Plots the bandpass of the input file INPFILE. The bandpass is plotted for
    the first timestamp in the file. The file can either be a FITS file or
    an LTA file - The type is determined by the filename extension.

    Inputs:
        inpfile     Name of the input file

    Returns:
        None
    """
    import subprocess
    import os
    import numpy as np
    import matplotlib.pyplot as plt

    from sys import stderr
    from astropy.io import fits
    from jansky.lib import fits as jfits
    from jansky.lib import antennas
    from jansky.lib import numeric

    if inpfile.lower().endswith('.lta'):
        is_lta = True
    else:
        is_lta = False # Assume to be FITS file

    if is_lta:
        # First run listscan to generate the log file
        subprocess.call(['listscan', inpfile])

        # Get the root of the "xxx.lta" filename, i.e., "xxx"
        filename = os.path.basename(inpfile)
        basename, ext = os.path.splitext(filename)

        logfile = basename + ".log"

        # Modify the logfile
        _parse_logfile(logfile)

        # Create FITS file
        subprocess.call(['gvfits', logfile])


    if not is_lta:
        if not inpfile.lower().endswith('.fits'):
            raise NameError('Input file should be either LTA or FITS format.')

    # At this point file is definitely FITS file
    if is_lta:
        fitsname = 'BSHAPE.FITS'
    else:
        fitsname = inpfile

    fitspars    = jfits.fitspars(fitsname)
    gcount      = fitspars.gcount
    chunk       = jfits.getfitschunk(gcount)

    # Since only first scan is being read, whole FITS file will fit in memory
    ffile       = fits.open(fitsname, memmap=True)

    basedat     = ffile[0].data[:].par('BASELINE')
    basedat     = antennas.base_aips_seq(basedat, doself=True)
    ant1, ant2  = antennas.base_to_ant(basedat, doself=True)
    basedat     = np.where(ant1 == ant2, basedat, 0)

    imgdat      = np.squeeze(ffile[0].data[:].data)

    basedat     = numeric.matchdims(basedat, imgdat.shape, end=True)
    # Keep only autocorrelation visibilities
    imgdat      = np.where(basedat, imgdat, 0)

    # Calculate amplitudes per polarization
    ampdat      = []
    for pp in range(fitspars.npol):
        ampdat.append(np.hypot(imgdat[...,pp,0], imgdat[...,pp,1]))

    ampdat      = np.array(ampdat)
    ampdat      = ampdat[ampdat != 0]
    ntime       = ampdat.shape[0]//(fitspars.nchan * fitspars.npol)

    ampdat      = ampdat.reshape(fitspars.npol, ntime, fitspars.nchan)

    # Overall normalization constant
    ampmedian   = np.median(ampdat)

    # Average over times
    imgmed      = np.median(ampdat, axis=1)

    fig, ax     = plt.subplots()

    fig.suptitle('Bandpass', fontsize=20)

    for pp in range(fitspars.npol):
        label = 'Polarization %d' % (pp + 1)
        ax.plot(imgmed[pp]/ampmedian, '.', label = label)

    plt.xlabel('Channels')
    plt.ylabel('Amplitude')
    plt.legend()
    plt.show()

    # Cleanup
    if is_lta:
        planfile = basename + ".plan"
        logfile  = basename + ".log"

        os.remove('BSHAPE.FITS')
        os.remove('gvfits.log')
        os.remove(planfile)
        os.remove(logfile)

