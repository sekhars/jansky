"""
Plot 1D and 2D histograms of the FITS file.
"""

from astropy.io import fits
import jansky.lib.fits as jfits
import jansky.lib.strings as strings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def _read_real(imgdat, poln):
    """
    Given the image array, returns the real values for the specified
    polarization
    """
    if poln == 1 or poln == 2:
        return np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 0], 0)
    else:
        return np.where(imgdat[..., 2], imgdat[..., 0], 0)


def _read_imag(imgdat, poln):
    """
    Given the image array, returns the imaginary values for the specified
    polarization
    """
    if poln == 1 or poln == 2:
        return np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 1], 0)
    else:
        return np.where(imgdat[..., 2], imgdat[..., 1], 0)


def _read_amp(imgdat, poln):
    """
    Given the image array, returns the ampliture values for the specified
    polarization
    """
    if poln == 1 or poln == 2:
        redat = np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 0], 0)
        imdat = np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 1], 0)
    else:
        redat = np.where(imgdat[..., 2], imgdat[..., 0], 0)
        imdat = np.where(imgdat[..., 2], imgdat[..., 1], 0)

    return np.hypot(redat, imdat)


def _read_phase(imgdat, poln):
    """
    Given the image array, returns the ampliture values for the specified
    polarization
    """
    if poln == 1 or poln == 2:
        redat = np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 0], 0)
        imdat = np.where(imgdat[..., poln-1, 2], imgdat[...,poln-1, 1], 0)
    else:
        redat = np.where(imgdat[..., 2], imgdat[..., 0], 0)
        imdat = np.where(imgdat[..., 2], imgdat[..., 1], 0)

    return np.arctan2(imdat, redat)


def _checkax(axis, axlist):
    if any([axis in ax for ax in axlist]):
        return True
    else:
        return False



def histogram(fitsfile, xaxis, yaxis, poln):
    """
    Inputs:
        fitsfile    Name of the input FITS file
        xaxis       Quantity to plot on the xaxis
        yaxis       Quantity to plot on the yaxis
        poln        Polarization to plot  {Any of 1,2,3}

    Returns:
        None

    Does the actual work of the function in fitsplot, i.e., fitsplot.histogram
    """

    fitspars    = jfits.fitspars(fitsfile)
    chunk       = jfits.getfitschunk(fitspars.gcount)

    axlist  = ['amplitude','real','imaginary','phase','uvlength','time']

    xaxis = xaxis.lower()
    if yaxis is not None:
        yaxis = yaxis.lower()

    if not _checkax(xaxis, axlist):
        errmsg = 'XAXIS must be one of %s' % axlist
        errmsg += "Maybe there's an extra space? Options are case insensitive'"
        raise ValueError(errmsg)

    if yaxis is not None:
        if not _checkax(yaxis, axlist):
            errmsg = 'YAXIS must be one of %s' % axlist
            errmsg += "Maybe there's an extra space?"
            errmsg += "Options are case insensitive'"
            raise ValueError(errmsg)

    nchan = fitspars.nchan
    # Frequency factor for UV values - They change by this factor per channel
    freqfactor  = np.array(1. +
            (fitspars.chanwidth * np.arange(nchan)/fitspars.refreq))
    if poln == 3:
        freqfactor = np.repeat(freqfactor, 2).reshape(freqfactor.shape[0], 2)

    strings.clearline()
    with fits.open(fitsfile, memmap=True) as ffile:
        uscale   = ffile[0].header['PSCAL1']
        vscale   = ffile[0].header['PSCAL2']

        for group in range(0, fitspars.gcount, chunk):
            print("Processing group", group, '/', fitspars.gcount, end="\r")

            if _checkax(xaxis, axlist[0:4]):
                imgdat = np.squeeze(ffile[0].data[group:group+chunk].data)

                if xaxis in 'real':
                    xdat = _read_real(imgdat, poln)
                elif xaxis in 'imaginary':
                    xdat = _read_imag(imgdat, poln)
                elif xaxis in 'amplitude':
                    xdat = _read_amp(imgdat, poln)
                elif xaxis in 'phase':
                    xdat = _read_phase(imgdat, poln)

            elif xaxis in 'time':
                xdat = ffile[0].data[group:group+chunk].par('DATE')

            elif xaxis in 'uvlength':
                uval = ffile[0].data[group:group+chunk].par(0)/uscale
                vval = ffile[0].data[group:group+chunk].par(1)/vscale
                xdat = np.hypot(uval, vval)


            if yaxis is not None:
                if _checkax(yaxis, axlist[0:4]):
                    imgdat = np.squeeze(ffile[0].data[group:group+chunk].data)

                    if yaxis in 'real':
                        ydat = _read_real(imgdat, poln)
                    elif yaxis in 'imaginary':
                        ydat = _read_imag(imgdat, poln)
                    elif yaxis in 'amplitude':
                        ydat = _read_amp(imgdat, poln)
                    elif yaxis in 'phase':
                        ydat = _read_phase(imgdat, poln)

                elif yaxis in 'time':
                    ydat = ffile[0].data[group:group+chunk].par('DATE')

                elif yaxis in 'uvlength':
                    uval = ffile[0].data[group:group+chunk].par(0)/uscale
                    vval = ffile[0].data[group:group+chunk].par[1]/vscale
                    ydat = np.hypot(uval, vval)


                # If one axis contains either UV or time and the other axis is
                # data, the number of channels needs to be accounted for,
                # since the data # is stored per channel and UV/Time is
                # only stored per baseline

                if _checkax(xaxis, axlist[0:4]) and _checkax(yaxis, axlist[4:]):
                    # yaxis needs to account for number of channels
                    yshape = ydat.shape
                    if poln == 3:
                        ydat = np.repeat(ydat, 2*nchan)
                        ydat = ydat.reshape(yshape[0], nchan, 2)
                    else:
                        ydat = np.repeat(ydat, nchan).reshape(yshape[0], nchan)

                    if yaxis in 'uvlength':
                        ydat = ydat * freqfactor

                if _checkax(yaxis, axlist[0:4]) and _checkax(xaxis, axlist[4:]):
                    # xaxis needs to account for number of channels
                    xshape = xdat.shape
                    if poln == 3:
                        xdat = np.repeat(xdat, 2*nchan)
                        xdat = xdat.reshape(xshape[0], nchan, 2)
                    else:
                        xdat = np.repeat(xdat, nchan).reshape(xshape[0], nchan)

                    if xaxis in 'uvlength':
                        xdat = xdat * freqfactor

            # Flatten out to 1D arrays
            xdat = xdat.flatten()
            if yaxis is not None:
                ydat = ydat.flatten()

            if yaxis is not None:
                recarr = np.rec.fromarrays((xdat, ydat),
                        dtype=[(xaxis, 'f8'), (yaxis, 'f8')])
            else:
                recarr = np.rec.fromarrays((xdat,), dtype=[(xaxis, 'f8')])

            # Create dataframe from arrays
            if group < chunk:
                dflist = []
                df = pd.DataFrame(recarr, dtype=np.float64)
                dflist.append(df)
            else:
                tf = pd.DataFrame(recarr, dtype=np.float64)
                dflist.append(tf)

    df = pd.concat(dflist)

    df = df.reset_index(drop=True)
    # Drop all rows that are zero
    df = df.loc[(df != 0).any(axis=1)]

    strings.clearline()
    print("Plotting...", end="\r")
    # Now all data has been accumulated
    plt.style.use('ggplot')

    from matplotlib.colors import LogNorm

    fig, ax = plt.subplots(1, 1)

    if yaxis is None:
        hist, bins = np.histogram(df[xaxis], bins=100)

        centre = (bins[:-1] + bins[1:])/2.
        width  = 0.95 * (bins[1] - bins[0])

        ax.bar(centre, hist, align='center', width=width, alpha=0.5)
        ax.set_xlabel(xaxis)

    if yaxis is not None:
        xlimits = [df[xaxis].min(), df[xaxis].max()]
        ylimits = [df[yaxis].min(), df[yaxis].max()]

        hist, xedge, yedge = np.histogram2d(x=df[xaxis], y=df[yaxis], bins=1000,
                range=(xlimits, ylimits))


        extent  = [xlimits[0], xlimits[1], ylimits[0], ylimits[1]]
        aspect  = (xlimits[1] - xlimits[0])/(ylimits[1] - ylimits[0])

        im = ax.imshow(hist.T, norm=LogNorm(), interpolation='none',
                origin='lower', cmap='Blues', extent=extent, aspect=aspect)

        ax.set_xlabel(xaxis)
        ax.set_ylabel(yaxis)
        ax.set_axis_bgcolor('black')
        ax.grid(False)
        plt.colorbar(im)

    strings.clearline()
    plt.show()
