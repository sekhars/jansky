#! /usr/bin/env python

"""
Module to hold baseline/antennas transformation/measurement stuff
"""
import jansky.lib.const as const
import numpy            as np
import numba            as nb


def ant_to_base(ant1, ant2, nantenna=const.NANTENNA, doauto=False):
    """
    Given two antennas returns the baseline number.

    Inputs:
        ant1        First antenna, must be less than nantenna
        ant2        Second antenna, must be less than nantenna
        nantenna    Total number of antennas in array - Default is defined
                    by the value NANTENNA in lib/const.py
        doauto      Include auto-correlation baselines? Default is False
    Returns:
        baseline    Baseline number
    """

    [ant1, ant2] = [min(ant1, ant2), max(ant1, ant2)]
    if ant1 > nantenna or ant2 > nantenna:
        errmsg = "Each antenna number must be less than the "
        errmsg += "total number of antennas."
        raise ValueError(errmsg)

    if doauto is True:
        baseline = ((ant1-1) * (2*nantenna - ant1)/2. + ant2)
    else:
        baseline = ((ant1-1) * (2*nantenna-ant1)/2. + ant2 - ant1)

    return int(baseline)


@nb.jit
def _getant(baseline, nantenna):
    """
    Function called by basetoant to do the actual calculations.

    Input:
        baseline    Input array/int of baseline numbers
        nantenna    Total number of antennas in array

    Returns
        ant1, ant2  Numpy array of constituent antennas
    """
    ant1 = np.zeros_like(baseline)
    ant2 = np.zeros_like(baseline)

    for index, bb in enumerate(baseline):
        tot = 0
        for nn in range(nantenna, 0, -1):
            tot = tot + nn
            if tot >= bb:
                ant1[index] = nantenna - nn + 1
                ant2[index] = nantenna + bb - tot
                break

    return ant1, ant2


def base_to_ant(baseline, nantenna=const.NANTENNA, doself=False):
    """
    Given an input baseline number (in sequential order, not in AIPS order)
    returns the two constituent antennas.

    Antenna numbering starts at 1, i.e., it is NOT zero indexed.

    Inputs:
        baseline    Baseline number, int
        nantenna    Number of antennas in array. By default it takes the
                    value defined in const.NANTENNA
        doself      Also account for auto-correlation baseline?
                    Default is False.
    Returns:
        ant1, ant2  Tuple of the constituent antennas
    """


    # Baseline is an array - Convert to ints
    if hasattr(baseline, '__len__'):
        baseline = np.array(baseline)
        baseline = baseline.astype(int)  # Make sure they are integers
    else:
        try:  # Try to coerce baseline to becoming an int, and create array
            baseline = int(baseline)
            baseline = np.array([baseline,])
        except TypeError:
            raise TypeError("baseline variable must be an integer type "
                    "or iterable of ints")


    if np.any(baseline < 1):
        raise ValueError("Baseline number cannot be less than 1.")

    if doself:
        if np.any(baseline > nantenna*(nantenna+1)/2.):
            raise ValueError("Baseline number exceeds maximum allowed number"
                    "of baselines.")
    else:
        if np.any(baseline > nantenna*(nantenna+1)/2.):
            raise ValueError("Baseline number exceeds maximum allowed number"
                    "of baselines.")
        nantenna = nantenna - 1


    ant1, ant2 = _getant(baseline, nantenna)

    if not doself:
        ant2 = ant2 + 1

    return ant1, ant2


def base_aips_seq(baseline, anttotal=const.NANTENNA, doself=False):
    """
    Converts the baseline numbers from AIPS format (258, 259... etc.) to
    sequential numbers (1, 2, ...).

    Input:
        baseline    Baseline number to convert - integer or array-like object
        anttotal    Total number of antennas in the interferometer
        doself      Input baselines contain self-baselines, bool

    Returns:
        seq_base    The sequential baseline number
    """

    # Baseline is an array - Convert to ints
    if hasattr(baseline, '__len__'):
        baseline = np.array(baseline)
        baseline = baseline.astype(int)  # Make sure they are integers
    else:
        try:  # Try to coerce baseline to becoming an int
            baseline = int(baseline)
        except TypeError:
            raise TypeError("baseline variable must be an integer type")

    ant1     = baseline // 256
    ant2     = baseline % 256

    if (np.any(ant1) < 1):
        raise ValueError("Antenna number cannot be less than 1.")

    if np.any(np.greater(ant1, ant2)):
        raise ValueError('ant1 <= ant2 should always hold.')

    if np.any(ant1 > anttotal) or np.any(ant2 > anttotal):
        raise ValueError('Antenna numbers found to be greated than'
                'total number of antennas.')

    if doself:
        seqbase = (ant1 - 1) * (2*anttotal - ant1)//2 + ant2
    else:
        seqbase = (ant1-1)*(2*anttotal - ant1)//2 + ant2 - ant1

    return seqbase
