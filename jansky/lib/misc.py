"""
Random bits of code that don't have a home.
"""

# Used to be in pyUVHist function but now that function only focuses only
# plotting rather than being a swiss army knife. So this fellow has moved here
# rather than starve out on the streets homeless.
def clipuv(ffile, limits, uvrange):
   """
   Clips points that fall outside the given limits.

   Inputs:
      ffile    Input FITS file object, astropy object
      limits   List of min/max limits to clip on, in the format
               ((minrep1, maxrep1),
                (minimp1, maximp1),
                (minrep2, maxrep2),
                (minimp2, maximp2),
                (minamp1, maxamp1),
                (minamp2, maxamp2),
                (minvre, maxvre),
                (minvim, maxvim),
                (minvamp, maxvamp))
      uvrange  UV limits within which to clip, [uvmin, uvmax] tuple of floats

   Returns:
      None
   """
   strings.clearline()

   gcount      = ffile[0].header['gcount']
   # Iterate through FITS file in chunks of `chunksize`
   chunksize   = jfits.getfitschunk(gcount)

   uscale   = ffile[0].header['PSCAL1']
   vscale   = ffile[0].header['PSCAL2']
   wscale   = ffile[0].header['PSCAL3']

   for group in range(1, gcount, chunksize):
      print("Clipping UV: Group", group, "/", gcount, end="\r")

      uval     = ffile[0].data.par(0)[group:group+chunksize]/uscale
      vval     = ffile[0].data.par(1)[group:group+chunksize]/vscale

      uvval    = np.hypot(uval, vval)
      # Pad array to match dimensions of imgdat
      uvval    = uvval[:, np.newaxis, np.newaxis, np.newaxis]

      imgdat   = np.squeeze(ffile[0].data[group:group+chunksize].data)
      # Select only within the UV range
      imgdat   = np.where((uvval > uvrange.lower) & (uvval < uvrange.upper),
                                                                     imgdat, 0)

      # Get pol 1
      datrep1  = np.where(imgdat[:,:,0,2], imgdat[:,:,0,0], 0)
      datimp1  = np.where(imgdat[:,:,0,2], imgdat[:,:,0,1], 0)
      datamp1  = np.hypot(datrep1, datimp1)

      # Get pol 2
      datrep2  = np.where(imgdat[:,:,1,2], imgdat[:,:,1,0], 0)
      datimp2  = np.where(imgdat[:,:,1,2], imgdat[:,:,1,1], 0)
      datamp2  = np.hypot(datrep2, datimp2)

      datvre   = datrep1 - datrep2
      datvim   = datimp1 - datimp2
      datvamp  = datamp1 - datamp2

      datrep1  = np.where((datrep1 > limits[0][0]) & (datrep1 < limits[0][1]),
                              datrep1, 0)
      datimp1  = np.where((datimp1 > limits[1][0]) & (datimp1 < limits[1][1]),
                              datimp1, 0)

      datrep2  = np.where((datrep2 > limits[2][0]) & (datrep2 < limits[3][1]),
                              datrep2, 0)
      datimp2  = np.where((datimp2 > limits[3][0]) & (datimp2 < limits[4][1]),
                              datimp2, 0)

      datamp1  = np.where((datamp1 > limits[4][0]) & (datamp1 < limits[2][1]),
                              datamp1, 0)
      datamp2  = np.where((datamp2 > limits[5][0]) & (datamp2 < limits[5][1]),
                              datamp2, 0)

      datvre   = np.where((datvre > limits[6][0]) & (datvre < limits[6][1]),
                              datvre, 0)
      datvim   = np.where((datvim > limits[7][0]) & (datvim < limits[7][1]),
                              datvim, 0)
      datvamp  = np.where((datvamp > limits[8][0]) & (datvamp < limits[8][1]),
                              datvamp, 0)

      imgdat[:,:,0,2] = np.where(datrep1 == 0, 0, imgdat[:,:,0,2])
      imgdat[:,:,0,2] = np.where(datimp1 == 0, 0, imgdat[:,:,0,2])
      imgdat[:,:,0,2] = np.where(datamp1 == 0, 0, imgdat[:,:,0,2])
      imgdat[:,:,1,2] = np.where(datrep2 == 0, 0, imgdat[:,:,0,2])
      imgdat[:,:,1,2] = np.where(datimp2 == 0, 0, imgdat[:,:,0,2])
      imgdat[:,:,0,2] = np.where(datamp2 == 0, 0, imgdat[:,:,0,2])

      # Pad with axes to match the initial unsqueezed shape
      imgdat = imgdat[:, np.newaxis, np.newaxis, np.newaxis, :,:,:]

      ffile[0].data[group:group+chunksize].base['DATA'] = imgdat
      ffile.flush()

      strings.clearline()


# See comment above above function. Also came from the (possibly) erstwhile
# pyUVHist.
def histlimits(hist, niter, nsigma):
   """
   Plot the various plots.

   Inputs:
     hist     List of histogram limits and bin edges. Look at the histuv
              function return value for the order.
     niter    Number of iterations to run sigma clipping, float
     nsigma   Sigma above which to reject outliers
   Returns:
     limits   Min/max limits after sigma clipping the histogram
   """

   # XXX Need to incorporate Sheppard's correction for variance/std dev.

   nn = 0

   # Convert histogram edges into bin centre values
   for ii in range(9):
      hist[ii][1] = stats.runningmean(hist[ii][1])

   while True:
      meanrep1    = stats.histmean(hist[0][0], hist[0][1])
      meanimp1    = stats.histmean(hist[1][0], hist[1][1])
      meanrep2    = stats.histmean(hist[2][0], hist[2][1])
      meanimp2    = stats.histmean(hist[3][0], hist[3][1])

      meanamp1    = stats.histmean(hist[4][0], hist[4][1])
      meanamp2    = stats.histmean(hist[5][0], hist[5][1])

      meanvre     = stats.histmean(hist[6][0], hist[6][1])
      meanvim     = stats.histmean(hist[7][0], hist[7][1])
      meanvamp    = stats.histmean(hist[8][0], hist[8][1])

      thismean    = [meanrep1, meanimp1, meanrep2, meanimp2, meanamp1,
                     meanamp2, meanvre, meanvim, meanvamp]

      if nn == 0:
         prevmean = np.copy(thismean)

      stdrep1     = stats.histstd(hist[0][0], hist[0][1])
      stdimp1     = stats.histstd(hist[1][0], hist[1][1])
      stdrep2     = stats.histstd(hist[2][0], hist[2][1])
      stdimp2     = stats.histstd(hist[3][0], hist[3][1])

      stdamp1     = stats.histstd(hist[4][0], hist[4][1])
      stdamp2     = stats.histstd(hist[5][0], hist[5][1])

      stdvre      = stats.histstd(hist[6][0], hist[6][1])
      stdvim      = stats.histstd(hist[7][0], hist[7][1])
      stdvamp     = stats.histstd(hist[8][0], hist[8][1])

      limrep1  = [meanrep1-nsigma*stdrep1, meanrep1+nsigma*stdrep1]
      limimp1  = [meanimp1-nsigma*stdimp1, meanimp1+nsigma*stdimp1]
      limrep2  = [meanrep2-nsigma*stdrep2, meanrep2+nsigma*stdrep2]
      limimp2  = [meanimp2-nsigma*stdimp2, meanimp2+nsigma*stdimp2]

      limamp1  = [0, meanamp1+nsigma*stdamp1]
      limamp2  = [0, meanamp2+nsigma*stdamp2]
      limvre   = [meanvre-nsigma*stdvre, meanvre+nsigma*stdvre]
      limvim   = [meanvim-nsigma*stdvim, meanvim+nsigma*stdvim]
      limvamp  = [meanvamp-nsigma*stdvamp, meanvamp+nsigma*stdvamp]

      # Get delta between the loops, if small enough then quit
      if nn > 0:
         deltamean   = np.subtract(thismean, prevmean)
         deltamean   = np.abs(deltamean)

         if np.all(deltamean < 0.01):
            break

         prevmean    = np.copy(thismean)

         if niter is not None:
            if nn >= niter:
               break
         elif np.all(np.less_equal(deltamean, 0.01)):
            break

      nn += 1

   retval = [limrep1, limimp1, limrep2, limimp2, limamp1, limamp2, limvre,
               limvim, limvamp]

   return retval


# Should be in lib/numeric.py. Meant to replace NumPy's `where` function but it
# is an order of magnitude slower. Need to figure out why that is. Until then
# it can continue to live here.
def where(mask, arr1, arr2):
    """
    A replacement for NumPy's `where` function, which uses less memory
    and intermediate arrays.

    Returns elements of ``arr1`` when ``mask`` is ``True`` and elements of
    ``arr2`` otherwise.

    ``arr1`` and ``arr2`` shapes should match exactly and the shape of
    ``mask`` should either match exactly or should be able to be padded to
    match. If ``mask`` has a larger/longer shape than ``arr1`` or ``arr2``
    an exception will be raised.

    Input:
        mask    The mask array to apply
        arr1    Return values from arr1 when mask is True
        arr2    Return values from arr2 otherwise

    Returns
        outarr  Same shape as ``arr1``
    """

    if arr1.shape != arr2.shape:
        raise ValueError("The shapes of arr1 and arr2 should match exactly.")

    if mask.shape[0] != arr1.shape[0]:
        raise ValueError("The shape of the mask array cannot be padded to"
        "match the input arrays")

    if len(mask.shape) < len(arr1.shape): # Pad to match shape
        didmatch = True
        mask = matchdims(mask, arr1.shape, end=True)
    elif len(mask.shape) > len(arr1.shape):
        raise ValueError("mask array has larger dimensions than input arrays.")

    else:
        if mask.shape != arr1.shape: # Same no. of dims, but does not match
            raise ValueError("Number of dimensions in mask array does not match"
            " input arrays, and cannot be padded to match input arrays")

        didmatch = False

    outarr  = np.zeros_like(arr1)

    @jit(nopython=True, cache=True)
    def _applymask_match(mask, arr1, arr2, outarr):
        for ii in range(mask.size):
            if mask[ii,...] == True:
                outarr[ii,...] = arr1[ii,...]
            else:
                outarr[ii,...] = arr2[ii,...]
        return outarr

    @jit(nopython=True, cache=True)
    def _applymask_nomatch(mask, arr1, arr2, outarr):
        for ii in range(arr1.size):
            if mask.flat[ii]:
                outarr.flat[ii] = arr1.flat[ii]
            else:
                outarr.flat[ii] == arr2.flat[ii]
        return outarr

    if didmatch is True:
        outarr = _applymask_match(mask, arr1, arr2, outarr)

    if didmatch is False:
        outarr = _applymask_nomatch(mask, arr1, arr2, outarr)

    return outarr
