# cython: boundscheck=False, wraparound=False, embedsignature=True
# cython: cdivision=True, language_level=3

import numpy as np
cimport numpy as np
cimport cython
from cython.parallel import prange
from libc.math cimport round as cround

ctypedef cython.floating flt # Fused type for double and float

"""
Holds various statistical functions to be compiled in Cython for that additional
speed boost.
"""

# This guy is about 3X faster than the numpy function. Perhaps there is more
# scope for improvement, but he's fast enough for now.
def histogram1d(flt[::1] data, flt[::1] limits, flt delta, int bins):
    """
    Calculates a 1 dimensional histogram. About 2-5X faster than np.histogram.

    Inputs:
        data    Input 1 dimensional data, Numpy array (double)
        limits  Lower and upper limits of histogram, Numpy array (double)
        delta   Bin width, double
        bins    Number of bins in the histogram, int

    Returns:
        hist    Histogram
    """

    cdef int ii, size, index
    cdef long[::1] hist = np.zeros(bins+2, dtype=np.int64)

    size = data.size

    if delta == 0:
        raise ValueError('Histogram delta cannot be zero.')

    with nogil:
        for ii in range(size):
            if data[ii] < limits[0]:  # Too low, zeroth bin
                index = 0
            elif data[ii] > limits[1]:  # Too high, last bin
                index = bins+1
            else:   # Goldilocks
                index = <int> ((data[ii] - limits[0])/delta) + 1

            hist[index] += 1

    return hist.base  # It's a memory view, so return the actual data



cpdef binary_search(double [:] data, double datp, int size):
    """
    Locate the index of ``datp`` in the input ``data`` using binary search.
    The input data is expected to be sorted and one dimensional.

    Inputs:
        data    Input data, double[:]
        datp    Data point to locate, double
        size    Size of the input array, int

    Returns:
        index   Index of datp
    """

    cdef int pivot, minv, maxv

    minv  = 0
    maxv  = size -1

    while minv <= maxv:
        pivot = <int> cround((maxv + minv)/2)

        if data[pivot] < datp:
            minv = pivot + 1
        elif data[pivot] > datp:
            maxv = pivot - 1
        else:
            return pivot

    return -1


# Define a fused type of only the real numbers, exclude complex
ctypedef fused real_nums:
    cython.int
    cython.long
    cython.float
    cython.double

cdef _c_min(real_nums a, real_nums b):
    """
    Return the minimum of a and b.

    Inputs:
        a, b    Input numbers, of same numeric type.

    Returns:
        min     Minimum value, same type as input
    """

    return a if a < b else b

cdef _c_max(real_nums a, real_nums b):
    """
    Return the minimum of a and b.

    Inputs:
        a, b    Input numbers, of same numeric type.

    Returns:
        min     Minimum value, same type as input
    """

    return a if a > b else b


cpdef _calc_surface_stats(double [:] windat, double datp, int subn):
    """
    Worker function for surface_stats - Actually calculates the
    stats.

    Inputs:
        windat  Valid data within the sliding window, 1D array of doubles
        datp    Data point around which to find statistics, double
        subn    The subset of values used to calculate the statistics, int

    Returns:
        mean    Mean of the window, double
        medn    Median of the window, double
        stdn    Standard deviation of the window, double
        range   Range in the window, double
        devn    Deviation of the selected point from the median
    """

    cdef double mean, medn, stdn, devn
    cdef double wrange, subrange
    cdef int ii, size, loc, lindex, uindex, endp
    cdef double [:] subw

    size = windat.size
    loc  = binary_search(windat, datp, size)

    # Some large number - Range in window
    wrange = 1E9

    endp    = _c_min(<double> size, <double> loc+subn)
    lindex  = loc
    uindex  = loc + subn

    # Get a compact window of size subn around the selected point.
    for ii in range(loc, endp):
        subrange = windat[ii+subn] - windat[ii]
        if wrange > subrange:
            wrange = subrange
            lindex = ii
            uindex = ii+subn

    mean = np.mean(windat[lindex:uindex])
    medn = windat[(lindex+uindex)/2]
    stdn = np.std(windat[lindex:uindex])
    devn = windat[loc] - medn

    return mean, medn, stdn, wrange, devn


def surface_stats(double [:,:] dat, int window, int[:] shape, float perc):
    """
    Runs the surface stats algorithm on the input 2D data.

    Inputs:
        dat     Input 2D data, 2D array of doubles
        window  Half size of the sliding window, int
        shape   Shape of the input array, tuple of 2 ints
        perc    Percentage of points within window to consider for stats, float

    Returns:
        meanv   Mean values, same shape as dat
        devnv   Deviation from median, same shape as dat
        stdv    Standard deviation values, same shape as dat
    """

    cdef double [:,:] mean = np.zeros_like(dat)
    cdef double [:,:] medn = np.zeros_like(dat)
    cdef double [:,:] stdn = np.zeros_like(dat)
    cdef double [:,:] rnge = np.zeros_like(dat)
    cdef double [:,:] devn = np.zeros_like(dat)

    cdef double [:] windat = np.zeros(2*(2*window + 1), dtype=np.float64)
    cdef double outval[3]

    cdef int ii, jj, iii, jjj, imin, imax, jmin, jmax, subn, size, npt

    subn = <int> np.floor(perc * shape[0] * shape[1])
    size = shape[0] * shape[1]

    for ii in range(shape[0]):
        # Stop at window edge
        imin = 0                if ii < window else ii - window
        imax = 2*window+1       if ii < window else ii + window
        imin = ii-(2*window+1)  if ii+window > shape[0] else imin
        imax = shape[0]         if ii+window > shape[0] else imax

        for jj in range(shape[1]):
            jmin = 0                if jj < window else jj - window
            jmax = 2*window+1       if jj < window else jj + window
            jmin = jj-(2*window+1)  if jj+window > shape[1] else imin
            jmax = shape[1]         if jj+window > shape[1] else imax

            # Read array into windat
            npt = 0
            for iii in range(imin, imax):
                for jjj in range(jmin, jmax):
                    if dat[iii,jjj]:
                        windat[npt] = dat[iii, jjj]
                        npt += 1

            if size < subn:
                subn = size

            if npt >= 10: # Some magic number below which we don't trust stats
                windat = np.sort(windat)
                outval = _calc_surface_stats(windat, dat[ii,jj], subn)

                mean[ii,jj] = outval[0]
                medn[ii,jj] = outval[1]
                stdn[ii,jj] = outval[2]
                rnge[ii,jj] = outval[3]
                devn[ii,jj] = outval[4]

    return mean.base, medn.base, stdn.base, rnge.base, devn.base


from jansky.lib import stats

def calc_rms_grid(flt [:,:] inparr, int window, int step):
    """
    Calculates the RMS over the 2D array, skipping every `step` points. The
    window used is to be square.

    Inputs:
        inparr  The input array, used to calculate the rms
        window  Size of the window used to calculate the RMS
        step    Step size between successive points that the RMS is being
                calculated for.

    Returns:
        rmsarr  Output array, where every `step` points contains the
                calculated RMS
    """

    cdef int            xx, yy, xsize, ysize, xmin, xmax, ymin, ymax
    cdef flt            std
    cdef flt[:,:]       rmsarr = np.zeros_like(inparr)

    xsize = inparr.shape[0]
    ysize = inparr.shape[1]

    for xx in range(0, xsize, step):
        xmin = _c_max[cython.int](0, xx-window)
        xmax = _c_min[cython.int](xx+window, xsize)

        for yy in range(0, ysize, step):
            ymin = _c_max[cython.int](0, yy-window)
            ymax = _c_min[cython.int](yy+window, ysize)

            std = stats.sigmaclipstats(inparr[xmin:xmax, ymin:ymax])[2]

            rmsarr[xx,yy] = std

    return rmsarr.base
