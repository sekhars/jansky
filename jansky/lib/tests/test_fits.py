#! /usr/bin/env python

import pytest
from ..              import fits
from numpy.testing   import assert_equal

"""
Test functions for the fits module
"""

def test_getfitschunk_1():
   chunksize = fits.getfitschunk(200000)
   assert_equal(chunksize, 100000)

def test_getfitschunk_2():
   chunksize = fits.getfitschunk(100000)
   assert_equal(chunksize, 100000)

def test_getfitschunk_3():
   chunksize = fits.getfitschunk(1000)
   assert_equal(chunksize, 1000)

def test_getfitschunk_4():
   chunksize = fits.getfitschunk(1)
   assert_equal(chunksize, 1)

def test_base_aips_seq_1():
    base = fits.base_aips_seq('C', 30, 258)
    assert_equal(base, 1)

def test_base_aips_seq_2():
    basearr = [258, 259]
    base = fits.base_aips_seq('C', 30, basearr)
    assert_equal(base, [1, 2])
