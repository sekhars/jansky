#! /usr/bin/env python

import pytest
from ..              import strings
from numpy.testing   import assert_allclose

"""
Test functions for the string module
"""

def test_stringToCoord_colon_1():
    ra = '23:45:52'
    dec = "18:54:11"
    coord = strings.stringToCoord(ra, dec)

    assert_allclose(coord.ra.value, 356.46666666)
    assert_allclose(coord.dec.value, 18.903055555555554)

def test_stringToCoord_colon_2():
    ra = '23:45:52.0'
    dec = "18:54:11.0"
    coord = strings.stringToCoord(ra, dec)

    assert_allclose(coord.ra.value, 356.46666666)
    assert_allclose(coord.dec.value, 18.903055555555554)

def test_stringToCoord_colon_3():
    ra = '23:45:52.0.0'
    dec = "18:54:11"
    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_stringToCoord_colon_4():
    ra = '23:45:52.0'
    dec = "18:54:11.1.18"
    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_stringToCoord_colon_5():
    ra = '23:45:52.0.0.12'
    dec = "18:54:11.1.18"
    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_stringToCoord_hms_1():
    ra = "23h45m52s"
    dec = "18d54m11s"

    coord = strings.stringToCoord(ra, dec)
    assert_allclose(coord.ra.value, 356.46666666)
    assert_allclose(coord.dec.value, 18.903055555555554)

def test_stringToCoord_hms_2():
    ra = "23h45m52.0s"
    dec = "18d54m11.0s"

    coord = strings.stringToCoord(ra, dec)
    assert_allclose(coord.ra.value, 356.46666666)
    assert_allclose(coord.dec.value, 18.903055555555554)

def test_stringToCoord_hms_3():
    ra = "23h45m52.0s"
    dec = "18h54m11.0s"

    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_stringToCoord_hms_4():
    ra = "23h45m52.0.12s"
    dec = "18h54m11.0s"

    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_stringToCoord_hms_5():
    ra = "23h45m52.0s.12"
    dec = "18h54m11.0s"

    with pytest.raises(ValueError):
        coord = strings.stringToCoord(ra, dec)

def test_minmax_onearg():
    inpstr  = "234"
    ret     = strings.minmax(inpstr)

    assert ret[0] == ret[1]

def test_minmax_twoargs_1():
    inpstr  = "118, 232"
    ret     = strings.minmax(inpstr)
    assert(ret[0] == 118)
    assert(ret[1] == 232)

def test_minmax_twoargs_2():
    inpstr  = "118.38, 232.52"
    ret     = strings.minmax(inpstr)
    assert(ret[0] == 118.38)
    assert(ret[1] == 232.52)

def test_minmax_string():
    inpstr = 'abcd'
    with pytest.raises(ValueError):
        ret = strings.minmax(inpstr)
