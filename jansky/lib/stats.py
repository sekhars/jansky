#! /usr/bin/env python
"""
Module to hold all the stats functions.
"""

import logging

from scipy.stats import sigmaclip as scsigmaclip
from astropy.stats import sigma_clip
from sys import exit

import numpy as np
import numba as nb
import jansky.lib.const as const

logger = logging.getLogger(__name__)


@nb.jit(nopython=True, nogil=True, cache=True)
def sigmaclip(inparr, sigma_low=3.0, sigma_high=3.0):
    """
    A modified version of the SciPy sigma clipping function to allow for
    a fixed number of iterations.

    Inputs:
        inparr      Input data, Numpy NDArray
        sigma_low   Lower sigma cutoff, default 3.0
        sigma_high  Upper sigma cutoff, default 3.0

    Returns:
        clipped_arr The sigma clipped array, flattened
    """

    delta = 1

    inparr = np.ravel(inparr)

    while delta:
        std = np.std(inparr)
        mean = np.mean(inparr)

        size = inparr.size

        low = mean - sigma_low * std
        high = mean + sigma_high * std

        inparr = inparr[(inparr > low) & (inparr < high)]

        delta = size - inparr.size

    return inparr


def sigmaclipstats(inparr, nsigma=3.0, sigma_low=None, sigma_high=None):
    """
    Run sigmaclipping on the input array and return the clipped array and the
    associated statistics.

    If both sigma_low and sigma_high are `None` then the nsigma value is used
    to clip on both the lower limit and upper limit. If only one of the two
    are specified, then the nsigma value is used for the other.

    Inputs:
        inparr      Input array, N dimensional NumPy array
        nsigma      Sigma value to clip above, default is 3.0
        sigma_low   Sigma value to clip on at lower limit, overrides the
                    nsigma value. Default is None
        sigma_high  Sigma value to clip on at the upper limits, overrides the
                    nsigma value, Default is None

    Returns:
        clipped_arr The sigma clipped array, N dimensional NumPy array
        mean        The robust mean, float
        median      The robust median, float
        std         The robust standard deviation, float
    """

    npt = np.count_nonzero(inparr)

    if npt < const.SIGMACLIP:  # Cannot run sigma clipping
        return inparr, 0, 0, -1

    if sigma_low is None:
        sigma_low = nsigma

    if sigma_high is None:
        sigma_high = nsigma

    clipped_arr = scsigmaclip(
        inparr, low=sigma_low, high=sigma_high)[0]

    mean = np.mean(clipped_arr)
    std = np.std(clipped_arr)
    median = np.median(clipped_arr)

    return clipped_arr, mean, std, median


def get_histbins(histlims, nbin, docentre=True, dooutlier=True):
    """
    Given the limits of a histogram and the number of bins, returns the
    locations of the bin centres, or alternatively the bin edges.

    Inputs:
        histlims    The limits of the histogram as (min, max), tuple-like
        nbin        The number of bins in the histogram, int
        docentre    Return the centre value of the bins rather than the edge
        dooutlier   If True, the first and last bins are outlier bins and the
                    input limits will apply from the second to the n-1th bin.

    Returns:
        histbins    The centre/edge values of the bins, numpy array of size nbin
    """

    delta = (histlims[1] - histlims[0]) / nbin

    # Calculate positions of the centres of bins rather than edges
    if docentre == True:
        beg = histlims[0] + delta / 2.
    else:
        beg = histlims[0]

    if docentre == True:
        end = histlims[1] + delta / 2.
    else:
        end = histlims[1]

    # This assumes that the first and last bin contain outliers.
    # Therefore if N bins are desired for the histogram, nbin should be N+2
    # to account for the outlier bins. That should be done outside the function
    if dooutlier == True:
        beg = beg - delta
        end = end + delta

    histbins = np.linspace(beg, end, nbin, endpoint=True)

    return histbins


def histmean(histcount, histbin):
    """
   Calculate the mean of a histogram given the bin value and the count
   per bin.

   Inputs:
      histcount   The count per bin, i.e., the y axis values on a histogram,
                  NumPy array
      histbin     The bin value, i.e., the x axis values on a histogram,
                  NumPy array

   Returns:
      histmean    The mean value of the histogram, float
   """

    # Total count of histogram
    nval = np.sum(histcount)
    # Sum of all elements, i.e., bin1*count1 + bin2*count2 + ... etc.
    sumval = np.sum(np.multiply(histcount, histbin))

    mean = sumval / nval

    return mean


def histstd(histcount, histbin):
    """
   Calculate the standard deviation of a histogram given the bin value and the
   count per bin.

   Inputs:
      histcount   The count per bin, i.e., the y axis values on a histogram,
                  NumPy array
      histbin     The bin value, i.e., the x axis values on a histogram,
                  NumPy array

   Returns:
      histstd     The standard deviation of the histogram, float
   """

    # Total count of the histogram
    nval = np.sum(histcount)

    # Sum of all elements, i.e., bin1*count1 + bin2*count2 + ... etc.
    sumval = np.sum(np.multiply(histcount, histbin))

    # Sum of square of all elements, i.e., bin1*count1**2 + bin2*count2**2 + ...
    histbin2 = histbin * histbin
    sum2val = np.sum(np.multiply(histcount, histbin2))

    mean = sumval / nval
    mean2 = sum2val / nval

    std = np.sqrt(mean2 - mean * mean)

    return std


def hist_stats_clipped(histcount, histbin, nsigma=3.0):
    """
    Given an input histogram, return the sigma clipped statistics. Outliers
    are rejected if they live outside (nsigma * standard deviation) from the
    mean.

    Inputs:
        histcount   The counts per bin i.e., the y axis values. Numpy array
        histbin     The bin value, i.e., the x axis values. Numpy array
        nsigma      Sigma above which outliers are clipped. float

    Returns:
        histmean    The sigma clipped mean of the histogram
        histstd     The sigma clipped standard deviation of the histogram
    """

    mean = histmean(histcount, histbin)
    std = histstd(histcount, histbin)

    upper_thresh = mean + nsigma * std
    lower_thresh = mean - nsigma * std

    cond1 = histcount[histbin < lower_thresh]
    cond2 = histcount[histbin > upper_thresh]

    while np.any(cond1) or np.any(cond2):
        histcount[histbin < lower_thresh] = 0
        histcount[histbin > upper_thresh] = 0

        if np.count_nonzero(histcount) < 10:  # Magic number
            cond1 = 0  # Too few points to run sigma clipping, so exit.
            cond2 = 0
            continue

        mean = histmean(histcount, histbin)
        std = histstd(histcount, histbin)

        upper_thresh = mean + nsigma * std
        lower_thresh = mean - nsigma * std

        cond1 = histcount[histbin < lower_thresh]
        cond2 = histcount[histbin > upper_thresh]

    return mean, std


def rolling_mean(dat, window=2.):
    """
   Calculate the running mean of an array. The mean is calculated within
   the window size `window`.

   Inputs:
      dat         Input array. If the input array is multidimensional, the
                  returned array will be the flattened version of the
                  N-dim array.
      window      Size of the window within which to calculate the mean
                  value

   Returns:
      rolling_mean The running mean along the flattened version of the input
                  array. The value in each array element is replaced by the
                  value of the local mean.
   """

    window = float(window)

    datmean = np.cumsum(dat)
    datmean[window:] = datmean[window:] - datmean[:-window]

    return datmean[window - 1:] / window


@nb.jit(nopython=True, nogil=True, cache=True)
def _std_1d(dat, window, step, do_sigmaclip, nsigma):

    stdarr = np.zeros(dat.shape)

    xsize = dat.shape[0]
    for ii in range(0, xsize, step):
        # Set bounds on the sliding window
        wminx = ii - window if ii > window else 0
        wmaxx = ii + window if ii > window else window
        # If previous condition takes it past the edge, reset it to the edge
        wminx = wminx if ii + window < xsize else xsize - 1 - window
        wmaxx = wmaxx if ii + window < xsize else xsize - 1

        winarr = dat[wminx:wmaxx]

        if do_sigmaclip:
            stdarr[ii] = np.std(sigmaclip(dat[wminx:wmaxx]))
        else:
            stdarr[ii] = np.std(dat[wminx:wmaxx])

    return stdarr


@nb.jit(nopython=True, nogil=True, cache=True)
def _std_2d(dat, window, step, do_sigmaclip, nsigma):
    xsize = dat.shape[0]
    ysize = dat.shape[1]

    stdarr = np.zeros(dat.shape)

    for ii in range(0, xsize, step):
        # Set bounds on the sliding window
        wminx = ii - window if ii > window else 0
        wmaxx = ii + window if ii > window else window
        # If previous condition takes it past the edge, reset it to the edge
        wminx = wminx if ii + window < xsize else xsize - 1 - window
        wmaxx = wmaxx if ii + window < xsize else xsize - 1

        for jj in range(0, ysize, step):
            # Set bounds on the sliding window
            wminy = jj - window if jj > window else 0
            wmaxy = jj + window if jj > window else window
            # If previous condition takes it past the edge, reset to edge
            wminy = wminy if jj + window < ysize else ysize - 1 - window
            wmaxy = wmaxy if jj + window < ysize else ysize - 1

            if do_sigmaclip:
                stdarr[ii, jj] = np.std(
                    sigmaclip(dat[wminx:wmaxx, wminy:wmaxy]))
            else:
                stdarr[ii, jj] = np.std(dat[wminx:wmaxx, wminy:wmaxy])

    return stdarr


def rolling_std(dat, window, step=1, do_sigmaclip=False, nsigma=3.):
    """
    Calculating the rolling standard deviation over a sliding window. It
    can optionally also calculate the standard deviation ever 'step' points.
    By default, step = 1 so the function will calculate the standard deviation
    at every point.

    Both window and step are assumed to be the same in each dimension.

    Only 1D and 2D data will work, the function cannot handle higher dimensions.

    Input:
       dat          Input data, ND-array (floats)
       window       Half size of the sliding window (int)
       step         Size of the step to skip over points [default: 1]
       do_sigmaclip Run sigma clipping before calculating std. dev.
       nsigma       Sigma at which to clip, if do_sigmaclip is True

    Returns:
       stdarr      Array containing the sliding standard deviation per point
    """

    dat = np.array(dat)

    ndim = len(dat.shape)

    if ndim > 2:
        raise ValueError("Only a maximum of two dimensions are supported.")

    if ndim == 1:
        stdarr = _std_1d(dat, window, step, do_sigmaclip, nsigma)

    if ndim == 2:
        stdarr = _std_2d(dat, window, step, do_sigmaclip, nsigma)

    return stdarr


@nb.jit(nopython=True, nogil=True, cache=True)
def consecutive_diff_1d(arr):
    """
    Calculates the difference between consecutive elements in a 1D array.
    Given a length N array, returns a length N-1 array since the last element
    cannot be differenced.

    Gives a 4-5x speedup on the equivalent function in NumPy (np.ediff1d)

    Inputs:
        arr     1 dimensional NumPy array
    Returns:
        outarr  Consecutive differences of input array
    """

    outarr = np.zeros(arr.shape[0] - 1)

    for ii in range(arr.shape[0] - 1):
        outarr[ii] = arr[ii + 1] - arr[ii]

    return outarr


def _check_limits_keyword(lim, dat, x_or_y=0):
    """
    Checks if the xlimits and ylimits keywords are okay. Called from the
    function histogram2d
    """

    if x_or_y == 0:
        name = 'xlimits'
    else:
        name = 'ylimits'

    errmsg = "Dimensions of %s keyword " % (name)
    errmsg += 'must be equal to 2'

    if lim is None:
        lim = [dat.min(), dat.max()]
    else:
        try:
            ndim = len(lim)
            if ndim > 2:  # Too long
                raise ValueError(errmsg)
        except TypeError:  # lim is an integer
            raise ValueError(errmsg)


@nb.jit(nopython=True, nogil=True, cache=True)
def _calc_histogram(xdat, xlimits, xdelta, ydat, ylimits, ydelta, bins,
                    hist2d):
    for ii in range(xdat.size):
        if xdat[ii] < xlimits[0]:
            xindex = 0
        elif xdat[ii] >= xlimits[1]:
            xindex = bins[0] + 1
        else:
            xindex = int((xdat[ii] - xlimits[0]) // xdelta) + 1

        if ydat[ii] < ylimits[0]:
            yindex = 0
        elif ydat[ii] >= ylimits[1]:
            yindex = bins[1] + 1
        else:
            yindex = int((ydat[ii] - ylimits[0]) // ydelta) + 1

        hist2d[xindex, yindex] += 1


def histogram2d(xdat, ydat, bins=100, xlimits=None, ylimits=None):
    """
    Computes the two dimensional histogram given two input data samples.

    Inputs:
        xdat    Input array containing data to be binned along X axis, 1 dim

        ydat    Input array containing data to be binned along Y axis, 1 dim

        bins    Number of bins in the histogram. Bins can either be an
                integer (ex: bins=1000) or it can be a tuple of two
                elements (ex: bins=[1000, 5000]) where each element of the
                tuple corresponds to the number of bins in the X and Y axis
                respectively.

        xlimits The limits of the histogram on the X axis. By default will use
                the minimum and maximum values of `xdat`.

        ylimits The limits of the histogram on the Y axis. By default will use
                the minimum and maximum values of `ydat`.

    Returns:
        hist2d  The two dimensional histogram, of dimensions `bins[0]xbins[1]`
                if `bins` is a tuple else `binsxbins` if `bins` is an integer.
    """

    if len(xdat.shape) > 1:
        raise ValueError('Input array must be one dimensional')

    if len(ydat.shape) > 1:
        raise ValueError('Input array must be one dimensional')

    if xdat.size != ydat.size:
        raise ValueError('Input arrays must be of same size')

    try:
        bindim = len(bins)
        if bindim > 2:
            raise ValueError('Dimensions of bins keyword '
                             'must not be greater than 2')
    except TypeError:
        bindim = 1
        bins = [bins, bins]

    # xlimits is modified within the function
    _check_limits_keyword(xlimits, xdat, x_or_y=0)
    _check_limits_keyword(ylimits, ydat, x_or_y=1)

    xdelta = (xlimits[1] - xlimits[0]) / bins[0]
    ydelta = (ylimits[1] - ylimits[0]) / bins[1]

    hist2d = np.zeros(bins, dtype=np.int)

    _calc_histogram(xdat, xlimits, xdelta, ydat, ylimits, ydelta, bins, hist2d)

    return hist2d


@nb.jit(nopython=True, nogil=True, cache=True)
def rolling_median(dat, window, step=1, do_sigmaclip=False, nsigma=3.):
    """
    Calculating the rolling median over a sliding window. It can optionally also
    calculate the median every 'step' points.  By default, step = 1 so the
    function will calculate the median at every point.

    Both window and step are assumed to be the same in each dimension.

    Only 1D and 2D data will work, the function cannot handle higher dimensions.

    Input:
       dat          Input data, ND-array (floats)
       window       Half size of the sliding window (int)
       step         Size of the step to skip over points [default: 1]
       do_sigmaclip Run sigma clipping before calculating median
       nsigma       Sigma at which to clip, if do_sigmaclip is True

    Returns:
       medarr      Array containing the median value per point
    """

    medarr = np.zeros(dat.shape)

    ndim = len(dat.shape)

    if ndim > 2:
        raise ValueError("Only a maximum of two dimensions are supported.")

    if ndim == 1:
        xsize = dat.shape[0]
        for ii in range(0, dat.size, step):
            # Set bounds on the sliding window
            wminx = ii - window if ii > window else 0
            wmaxx = ii + window if ii > window else window
            # If previous condition takes it past the edge, reset it to the edge
            wminx = wminx if ii + window < xsize else xsize - 1 - window
            wmaxx = wmaxx if ii + window < xsize else xsize - 1

            winarr = dat[wminx:wmaxx]

            if do_sigmaclip:
                medarr[ii] = np.median(sigmaclip(dat[wminx:wmaxx]))
            else:
                medarr[ii] = np.median(dat[wminx:wmaxx])

    if ndim == 2:
        xsize = dat.shape[0]
        ysize = dat.shape[1]
        for ii in range(0, dat.shape[0], step):
            # Set bounds on the sliding window
            wminx = ii - window if ii > window else 0
            wmaxx = ii + window if ii > window else window
            # If previous condition takes it past the edge, reset it to the edge
            wminx = wminx if ii + window < xsize else xsize - 1 - window
            wmaxx = wmaxx if ii + window < xsize else xsize - 1

            for jj in range(0, dat.shape[1], step):
                # Set bounds on the sliding window
                wminy = jj - window if jj > window else 0
                wmaxy = jj + window if jj > window else window
                # If previous condition takes it past the edge, reset to edge
                wminy = wminy if jj + window < ysize else ysize - 1 - window
                wmaxy = wmaxy if jj + window < ysize else ysize - 1

                if do_sigmaclip:
                    medarr[ii, jj] = np.median(
                        sigmaclip(dat[wminx:wmaxx, wminy:wmaxy]))
                else:
                    medarr[ii, jj] = np.median(dat[wminx:wmaxx, wminy:wmaxy])

    return medarr
