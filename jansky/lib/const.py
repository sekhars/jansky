#! /usr/bin/env python

"""
Module that holds all the interferometer-related and physical constants.
"""

# Number of antennas
NANTENNA = 30
# Number of baselines (cross only)
NBASELINE  = (NANTENNA * (NANTENNA-1))//2
# Number of baselines including self
NBASELINEA = (NANTENNA * (NANTENNA+1))//2
# Speed of light
C = 299792458
# Number of seconds in one solar day
SOLARDAY_SEC = 86400.0
# Minimum number of points for sigma clipping
SIGMACLIP = 5
