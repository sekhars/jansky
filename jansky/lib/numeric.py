#! /usr/bin/env python

"""
Module to hold extensions to NumPy/SciPy and other functions that act on
numeric data that isn't necessarily a FITS file or I/O operation
"""

import numpy as np

def interleave(arrays):
   """
   Given N arrays, interleave them such that the first dimension is the
   sum of the number of elements in the first dimension of all the input arrays,
   and the rest of the dimensions are the same.
   Rows from a particular array will appear after len(arrays) rows in the
   output, interleaved array.

   Ex: If two arrays A & B are input, the interleaved rows will look like
   r_A, r_B, r_A, r_B... where r_A is a row from A and r_B is a row from B.

   If three arrays A,B & C are input, the interleaved array will look like
   r_A, r_B, r_C, r_A, r_B, r_C... etc.

   NOTE: This can work on arbitrary n-dimension arrays, only the first dimension
   will be interleaved.

   Input:
      arrays      List of N-D arrays all of equal dimension and shape

   Returns:
      leaved_arr  Interleaved array, whose first dimension is as specified above
   """

   # Number of arrays
   n_arr = len(arrays)

   dim = 0
   # Check if array shapes match
   for nn in range(n_arr):
      if arrays[0].shape != arrays[nn].shape:
         raise ValueError("Shapes of input arrays do not match")

      # Calculate the total size of the first dimension
      dim += arrays[nn].shape[0]

   # Set the shape of the return array
   shape = [dim,]
   shape.extend(arrays[0].shape[1:])

   leaved   = np.zeros(shape)

   for nn in range(n_arr):
      leaved[nn::n_arr, :] = arrays[nn]

   return leaved


def matchdims(inparray, shape, end=False):
    """

    Given an input numpy array, matches it's dimensions to `shape` by
    padding it with `np.newaxis`. By default the padded axes are added
    to the beginning of the input array, but if `end=True` the new axes
    will be added to the end of the input array.

    Input:
        inparray    Array which requires padding, NumPy array
        shape       The shape to match, tuple

    Returns:
        padarray    Input array with padded dimensions
    """

    niter = len(shape) - len(inparray.shape)

    if end is False:
        for ii in range(niter):
            inparray = inparray[np.newaxis, ...]
    else:
        for ii in range(niter):
            inparray = inparray[..., np.newaxis]

    return inparray
