#! /usr/bin/env python

"""
Module to do string manipulations, mostly convenience wrappers around
other more useful functions
"""

from collections            import namedtuple

import re
import logging

logger   = logging.getLogger(__name__)


def string_to_coord(inp_ra, inp_dec):
    """
    Given an input string will try to convert it into an astropy coordinate
    object.

    Inputs:
       inp_ra      Input RA string
       inp_dec     Input DEC string

    Returns:
       coord       Astropy coordinate object, with RA and DEC.

    The input strings can be of the format HH:MM:SS, DD:MM:SS or
    XXhXXmXXs XXdXXmXXs. If the input string is neither, the function will
    complain and quit.
    """
    from astropy.coordinates   import SkyCoord

    # Define the regex to match either XX:XX:XX or XXhXXmXXs or XXdXXmXXs
    colondelim = re.compile("^[\w +-]\d+?:\d+?:\d+?(\.\d+?)?$")
    hmsdelim   = re.compile("^[\w +-]\d+?(h|d)\d+?m\d+?(\.\d+?)?s*$")

    # Check if the regex matches successfully
    if colondelim.match(inp_ra) is not None:
       inp_ra = inp_ra.split(':')
       inp_ra = inp_ra[0]+'h'+inp_ra[1]+'m'+inp_ra[2]+'s'

    if colondelim.match(inp_dec) is not None:
       inp_dec = inp_dec.split(':')
       inp_dec = inp_dec[0]+'d'+inp_dec[1]+'m'+inp_dec[2]+'s'

    # List to hold the match values, for quick checking later
    match = [hmsdelim.match(inp_ra), hmsdelim.match(inp_dec)]

    # Create the coordinates, or complain and quit
    if all([mm is not None for mm in match]):
       coord = SkyCoord(ra=inp_ra, dec=inp_dec)
       logging.info("Successfully converted string to coordinates")
    else:
       errormsg = "RA and DEC coordinates must either be of the form \
       HH:MM:SS DD:MM:SS or XXhXXmXXs XXdXXmXXs."
       raise ValueError(errormsg)

    return coord


def clearline(size=80):
    """
    Clears a line that is terminated with a \r character rather than a newline
    so that trailing characters don't hang around the ends of the line

    Inputs:
        size     Length of the line to clear out

    Returns:
        None
    """

    print("", end="\r")

    for nn in range(size):
        print(" ", end="")

    print("", end="\r")


Limits = namedtuple('Limits', 'min, max')
def minmax(inp, sep=','):
    """
    Given an input delimited string (comma delimited by default), returns
    the minimum and maximum values as floats.

    Inputs:
        inp     Input delimited string
        sep     Delimiter, default=','

    Returns:
        minmax  List of two elements, containing minimum and maximum values
                in the input string
    """

    vals    = [float(val) for val in inp.split(sep)]

    minmax  = [min(vals), max(vals)]
    lim     = Limits(minmax[0], minmax[1])

    return lim


def getslice(inpstr):
    """
    Given an input string with a slice-like syntax, return a slice object.
    Valid syntax includes '2:3', '0:8:2', ':-1'

    Inputs:
        inpstr      Input string with slice-like syntax

    Returns:
        outslice    Slice object
    """

    tokens = inpstr.split(':')

    if inpstr[0] == ':':
        end_slice = True
    else:
        end_slice = False

    if inpstr[0] == ':' and inpstr[1] == ':':
        only_step = True
    else:
        only_step = False

    out_token = []
    for tkn in tokens:
        try:
            out_token.append(int(tkn))
        except ValueError:
            pass

    if len(out_token) == 1:
        if end_slice == False:
            outslice = slice(out_token[0], None)
        else:
            outslice = slice(None, out_token[0])

        if only_step == True:
            outslice = slice(None, None, out_token[0])

    if len(out_token) == 2:
        if end_slice == False:
            outslice = slice(out_token[0], out_token[1])
        else:
            outslice = slice(None, out_token[0], out_token[1])

    if len(out_token) == 3:
            outslice = slice(out_token[0], out_token[1], out_token[2])

    return outslice
