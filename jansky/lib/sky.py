#! /usr/bin/env python

"""
Module that contains function to do various transformations on sky coordinates/
image plane/objects in the image plane.
"""

from astropy.coordinates import SkyCoord
from astropy import units

import numpy as np
import logging


def get_offset(coord1, coord2):
    """
    Given two input coordinates, will calculate the RA and DEC offset
    separately - FROM coord1 TO coord2. Swapping the two coordinates will
    give a completely different answer.

    Inputs:
        coord1, coord2    Input coordinates, Astropy SkyCoord objects

    Returns:
        offset            Offsets in arcsec, tuple of floats
    """

    ra_offset = (coord2.ra - coord1.ra) * np.cos(coord1.dec.to('radian'))
    dec_offset = (coord2.dec - coord1.dec)

    offset = [ra_offset.value * 3600.0, dec_offset.value * 3600.0]

    return offset


def radec_to_lmn(ref, obj):
    """
    Function to convert offsets in RA, DEC to l, m, n coordinates.
    The conversions come from Thompson, Moran & Swenson - 2nd Ed (2004)
    Chapter 4.

    Inputs:
        ref   Reference coordinates - Astropy SkyCoord object
        obj   Coordinates of object that lmn is to be calculated for,
             Astropy SkyCoord object

    Returns:
        lmn   The value of the direction cosines, Numpy array
    """

    logger = logging.getLogger(__name__)

    deltaRA = obj.ra - ref.ra

    l = np.cos(obj.dec) * np.sin(deltaRA)
    if ref.dec != 0:
        m = (np.sin(obj.dec) * np.cos(ref.dec) -
             np.cos(obj.dec) * np.sin(ref.dec) * np.cos(deltaRA))
    else:
        m = 0

    n = np.sqrt(1 - l**2 - m**2)

    lmn = np.array([l, m, n])

    logger.info("Obtained lmn coordinates")

    return lmn


def visibility(uvw, lmn, flux=1):
    """
    Function generates the visibility value given the UVW values for the
    baseline, and the lmn values for the source.

    Inputs:
        uvw      Numpy Array of uvw values, uvw[0] = u, uvw[1] = v, uvw[2] = w
        lmn      Numpy array of lmn values, lmn[0] = l, lmn[1] = m, lmn[2] = n
        flux     Flux of the source, default is 1 Jy - Float

    Returns:
        vis      Value of the visibility - Float
    """

    phase = 2. * np.pi * (uvw[0] * lmn[0] + uvw[1] *
                          lmn[1] + uvw[2] * (lmn[2] - 1))

    vis = flux * (np.cos(phase) + 1j * np.sin(phase))

    return vis
