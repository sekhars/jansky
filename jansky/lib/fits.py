#! /usr/bin/env python

"""
Module which house all the FITS I/O routines - Apart from the ones in Astropy
"""

from astropy.io            import fits
from collections           import namedtuple
from jansky.lib.stats      import sigmaclipstats

import numpy               as np
import jansky.lib.const    as const
import jansky.lib.stats    as stats

import logging

logger = logging.getLogger(__name__)

def fitspars(fitsname):
   """
   Returns the values of the various FITS parameters
   """
   ffile    = fits.open(fitsname, memmap=True)

   gcount   = ffile[0].header['gcount']
   pcount   = ffile[0].header['pcount']
   naxis    = ffile[0].header['naxis']

   # Get the number of channels in a flexible manner
   for ii in range(2, naxis+1):
       key = ffile[0].header['CTYPE'+str(ii)]
       if 'FREQ' in key:
           chanval = ii
           break

   # Get the number of polarizations in a flexible manner
   for ii in range(2, naxis+1):
       key = ffile[0].header['CTYPE'+str(ii)]
       if 'STOKES' in key:
           stokesval = ii
           break

   nchan       = ffile[0].header['naxis' + str(chanval)]
   chanwidth   = ffile[0].header['CDELT' + str(chanval)] # In Hz
   refreq      = ffile[0].header['CRVAL' + str(chanval)]
   refpix      = ffile[0].header['CRPIX' + str(chanval)]
   # We want the frequency at the first pixel
   refreq      = refreq - (refpix - 1)*chanwidth

   # Number of polarizations
   npol        = ffile[0].header['naxis' + str(stokesval)]

   refreq      /= 1E6 # Change to MHz
   chanwidth   /= 1E6 # Change to MHz

   # Loop through the header to find the value of gelements - NAXIS1 is always 0
   gelements = 1
   for ii in range(2, naxis):
      gelements *= ffile[0].header['naxis'+str(ii)]

   ffile.close()

   fitspars = namedtuple('fitspars', ['gcount', 'pcount', 'nchan',
                                 'chanwidth', 'refreq', 'gelements', 'npol'])
   retval   = fitspars(gcount, pcount, nchan, chanwidth, refreq,
                                                             gelements, npol)
   return retval


def getfitschunk(gcount):
   """
    Determines a chunk size to use to iterate over the FITS file, given
    the total group count.

    Args:
        gcount      Total number of groups in the FITS file (int)
    Returns:
        chunksize   Chunksize to use (int)
   """

   # Set a maximum chunksize of 10,000 reducing in steps of 1000
   chunksize = 100000
   try:
      while gcount / chunksize < 1:
         chunksize -= 1000
   except ZeroDivisionError:  # gcount is smaller than 1000
      chunksize = 1

   return chunksize


def getuvlim(fitsname):
   """
   Returns the values of the min and max UV values in a FITS file

   fitsname    Name of input FITS file  (string)
   """

   ffile = fits.open(fitsname, memmap=True)

   # Get scaling factors for U, V - Need to divide out to get the
   # UV values in lambda
   uscale   = ffile[0].header['PSCAL1']
   vscale   = ffile[0].header['PSCAL2']
   wscale   = ffile[0].header['PSCAL3']

   gcount   = ffile[0].header['gcount']

   chunksize = getfitschunk(gcount)

   maxu = maxv = maxw = maxuv = 0
   minu = minv = minw = minuv = 0
   # Iterate through FITS file in steps of chunksize
   for group in range(1, gcount, chunksize):
      print("Reading group ", group, "/", gcount, end="\r")

      uval  = ffile[0].data.par(0)[group:group+chunksize]/uscale
      vval  = ffile[0].data.par(1)[group:group+chunksize]/vscale
      wval  = ffile[0].data.par(2)[group:group+chunksize]/wscale

      maxu  = np.maximum(maxu, np.amax(uval))
      maxv  = np.maximum(maxv, np.amax(vval))
      maxw  = np.maximum(maxw, np.amax(wval))
      maxuv = np.maximum(maxuv, np.amax(np.hypot(uval, vval)))

      minu  = np.minimum(minu, np.amin(uval))
      minv  = np.minimum(minv, np.amin(vval))
      minw  = np.minimum(minw, np.amin(wval))
      minuv = np.minimum(minuv, np.amin(np.hypot(uval, vval)))

   ffile.close()

   UVLimits = namedtuple('UVLimits', ['umin', 'umax', 'vmin', 'vmax',
                                          'wmin', 'wmax', 'uvmin', 'uvmax'])

   retval = UVLimits(minu, maxu, minv, maxv, minw, maxw, minuv, maxuv)

   return retval

def fitsintegration(fitsname):
   """
   Given an input FITS file, calculates the integration time and jitter.

   Inputs:
      fitsname    Name of the input FITS file

   Returns:
      integration Integration time in seconds
      jitter      Jitter in the integration time
   """

   ffile = fits.open(fitsname, memmap=True)

   while True:
      baseline = 258
      # Get all the timestamps of a single baseline
      basetimes = np.where(ffile[0].data[:].par('BASELINE') == baseline,
                                             ffile[0].data[:].par('DATE'), 0)

      # Throw away all values that are zero
      basetimes = basetimes[basetimes > 0]

      # Min 30 points for statistics, else search the next baseline
      if (basetimes.size > 30):
         break

      baseline = baseline + 1

   ffile.close()

   # Convert from julian day to seconds
   basetimes   = basetimes * const.SOLARDAY_SEC

   # Difference between successive elements
   basediff    = np.diff(basetimes)

   # Sigmaclip and find statistics
   mean, median, std = sigmaclipstats(basediff)[1:]

   return mean, median, std


def getref_freq(fitsname):
   """
   Function that returns the reference frequency at pixel 0 as defined in the
   FITS header file.

   Args:
      fitsname Name of the input FITS file

   Returns:
      refreq   Reference frequency at pixel zero, float
      refdelt  Delta frequency per pixel/frequency channel
   """

   ffile = fits.open(fitsname, memmap=True)

   # Iterate through the header dictionary
   for key, val in ffile[0].header.items():
      if val == 'FREQ': # We've got the right keyword
         newkey   = key.replace('CTYPE', 'CRVAL')
         ffreq    = ffile[0].header[newkey]  # Get frequency

         newkey   = newkey.replace('CRVAL', 'CDELT')
         refdelt  = ffile[0].header[newkey]  # Get delta frequency

         newkey   = newkey.replace('CDELT', 'CRPIX')
         refpix   = ffile[0].header[newkey]

         break

   if refpix >= 1:
      refreq   = ffreq - (refpix - 1) * refdelt
   elif refpix == 0:
      refreq   = ffreq
   else:
      print("Error. Reference pixel of 'FREQ' keyword", end="", file=stderr)
      print(" is negative. Exiting", file=stderr)

      logger.error("CRPIX keyword of FREQ keyword is negative")
      logger.error("This should normally not happen. Exiting.")
      exit(1)

   ffile.close()

   return refreq, refdelt


def checkcomplete(fitsname, nbaseline=const.NBASELINE):
    """
    Checks if the input FITS file is complete i.e., if all the baselines
    are present for all timestamps.

    Inputs:
        fitsname    Name of the input FITS file, str
        nbaseline   Number of baselines in array, int

    Returns:
        iscomplete  True indicates FITS is complete, bool
    """

    nbaseline = int(nbaseline)

    from jansky.lib.antennas import base_aips_seq

    fitspar     = fitspars(fitsname)
    gcount      = fitspar.gcount
    chunk       = getfitschunk(gcount)

    ffile       = fits.open(fitsname, memmap=True)

    for group in range(0, gcount, chunk):
        print("Checking FITS completeness group ", group, "/", gcount, end="\r")

        basedat = ffile[0].data[group:group+chunk].par('BASELINE')

        if np.any(basedat[0:10] // 256): # AIPS format baselines
            basedat = base_aips_seq(basedat, doself=False)

        index = 0
        for bb in range(nbaseline):
            basediff = np.diff(basedat[index:index+nbaseline])

            if np.any(basediff > 1):
                raise ValueError("FITS file is not complete")

            index += nbaseline

    ffile.close()

def scan_break(fitsfile, break_width=180):
    """
    Given an input fitsfile, return a list of group numbers where there is a
    scan break. A scan break is defined as a gap in the time between two groups
    greater than break_width.

    Inputs:
        fitsfile        Name of input FITS file, str
        break_width     Width of scan break, int

    Returns:
        scan_begs       List of groups that are the beginning of scans, ndarray
        scan_ends       List of groups that are the end of scans, ndarray
    """

    pars     = fitspars(fitsfile)
    chunk    = getfitschunk(pars.gcount)

    group_beg = [0,]
    group_end = []

    with fits.open(fitsfile, memmap=True) as ffile:
        for group in range(0, pars.gcount, chunk):
            date = ffile[0].data[group:group+chunk].par('DATE')
            date *= const.SOLARDAY_SEC
            date_diff = stats.consecutive_diff_1d(date)

            indices = np.where(date_diff >= break_width)[0]

            group_beg.extend(group + indices + 1)
            group_end.extend(group + indices)

    group_end.append(pars.gcount)
    return np.array(group_beg), np.array(group_end)

def reset_fits(fitsfile, val=1E-6):
    """
    Given an input FITS file, replaces all the values with val.

    Inputs:
        fitsfile    Name of input FITS file, str
        val         Value to write into FITS file, float

    Returns:
        None        Modifications to FITS file are done in place.
    """

    this_pars = fitspars(fitsfile)
    gcount    = this_pars.gcount
    chunk     = getfitschunk(gcount)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:
        for group in range(0, gcount, chunk):
            imgdat = ffile[0].data[group:group+chunk].data

            imgdat[...,2] = 1.  # unflag all the points
            imgdat[...,0] = val # Reset the visibility value
            imgdat[...,1] = val

            ffile[0].data[group:group+chunk].base['DATA'] = imgdat

        ffile.flush()
