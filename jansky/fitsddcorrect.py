import click
import numpy as np
import jansky.lib.fits as jfits
import jansky.lib.strings as strings

from astropy.io import fits


def get_source_pos(srclist):
    """
    Function that will read the input source file with coordinates and
    calculate the offset from phase centre.

    Input:
       srclist        Name of the input file

    Returns:
       poslist        Positions of each source, Astropy SkyCoord object list
    """

    radec = []  # Empty list
    # Open file as readonly
    with open(srclist, 'r') as fptr:
        for line in fptr:
            # Skip commented and blank lines
            if line[0] == '#' or line.strip() == '':
                continue

            coords = line.split()
            coords = strings.string_to_coord(coords[0], coords[1])
            radec.append(coords)

    return radec


def _checkperiod(fringe):
    """
    Given an input fringe, checks if it has one full period. The input should
    be a one dimensional array.

    Input:
        fringe      Fringe to check, 1D NumPy array
    Returns:
        True/False  True if fringe contains at least one full period
    """

    sum_period = allclose = False

    # The below expression will be equal to either 2 or -2 when the sign flips
    sign = np.diff(np.sign(fringe))

    # We are looking for at least two sign changes for a full period.
    if np.sum(np.abs(sign)) > 4:
        sum_period = True

    maxval = np.max(fringe)
    minval = np.min(fringe)

    # Check if maxval and minval are within ten percent of each other
    if (np.abs(maxval) - np.abs(minval)) <= 0.1 * np.amax(
        [0.1, np.abs(maxval), np.abs(minval)]):
        allclose = True

    if allclose and sum_period:
        return True

    return False


def gensin(lmn, uvw, amp=1., phase=0.):

    phase_v = 2. * np.pi * (uvw[0] * lmn[0] + uvw[1] * lmn[1] + uvw[2] *
                            (lmn[2] - 1))
    sin = amp * np.sin(phase_v + phase)

    return sin


def gencos(lmn, uvw, amp=1., phase=0.):

    phase_v = 2. * np.pi * (uvw[0] * lmn[0] + uvw[1] * lmn[1] + uvw[2] *
                            (lmn[2] - 1))
    cos = amp * np.cos(phase_v + phase)

    return cos


def getampphase(fitamp):
    """
    Converts the fitted amplitudes of the form a*sin(x) + b*cos(x) to
    an amplitude an phase of the form A*sin(x+p) where A = sqrt(a**2 + b**2)
    and p = atan2(b, a).

    Inputs:
        fitamp      Input amplitudes, for all sources. Size is nsource*2
        nsource     The number of sources

    Returns
        amp, phase  Calculated amplitude and phase
    """

    amp = np.hypot(fitamp[0], fitamp[1])
    phase = np.arctan2(fitamp[1], fitamp[0])

    return amp, phase


def subtract_channel(uvw, freqfactor, bb, basedat, nsource, srclmn, nn,
                     modeldat, outfile, amplim, date, doplot, savepath):
    """
    Run the fringe fitting per channel and return the fittedamplitudes
    and phases.
    """

    from jansky.lib import stats

    if doplot:
        import matplotlib.pyplot as plt
        import matplotlib.dates as mdates
        from astropy.time import Time

    baseu = uvw[0] * freqfactor
    basev = uvw[1] * freqfactor
    basew = uvw[2] * freqfactor

    datrep1 = np.where(basedat[nn, :, 0, 2], basedat[nn, :, 0, 0], 0)
    datimp1 = np.where(basedat[nn, :, 0, 2], basedat[nn, :, 0, 1], 0)
    datrep2 = np.where(basedat[nn, :, 1, 2], basedat[nn, :, 1, 0], 0)
    datimp2 = np.where(basedat[nn, :, 1, 2], basedat[nn, :, 1, 1], 0)

    if doplot:
        cumlfit = np.zeros_like(datrep1)

    for ss in range(nsource):
        models = gensin(srclmn[ss], (baseu, basev, basew))
        modelc = gencos(srclmn[ss], (baseu, basev, basew))

        # Only fit fringes with a full period
        if not all([_checkperiod(models), _checkperiod(modelc)]):
            if outfile is not None:
                # Fill rest of the columns with zero so the file is consistent
                outfile.write('{: 04d} {: 04d}'.format(bb + 1, nn + 1))
                outfile.write('{: 08f}'.format(np.median(date)))
                outfile.write('{: 04d}'.format(ss + 1))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('{: 10.5f} {: 10.5f}'.format(0, 0))
                outfile.write('\n')

            continue

        model = np.vstack([models, modelc])

        amprep1 = np.linalg.lstsq(model.T, datrep1)[0]
        ampimp1 = np.linalg.lstsq(model.T, datimp1)[0]
        amprep2 = np.linalg.lstsq(model.T, datrep2)[0]
        ampimp2 = np.linalg.lstsq(model.T, datimp2)[0]

        arep1, prep1 = getampphase(amprep1)
        aimp1, pimp1 = getampphase(ampimp1)
        arep2, prep2 = getampphase(amprep2)
        aimp2, pimp2 = getampphase(ampimp2)

        # Smaller than the amplitude cutoff
        if arep1 < amplim:
            arep1 = 0
        if aimp1 < amplim:
            aimp1 = 0
        if arep2 < amplim:
            arep2 = 0
        if aimp2 < amplim:
            aimp2 = 0

        if outfile is not None:
            outfile.write('{: 04d} {: 04d}'.format(bb + 1, nn + 1))
            outfile.write('{: 08f}'.format(np.median(date)))
            outfile.write('{: 04d}'.format(ss + 1))
            outfile.write('{: 10.5f} {: 10.5f}'.format(arep1, prep1))
            outfile.write('{: 10.5f} {: 10.5f}'.format(aimp1, pimp1))
            outfile.write('{: 10.5f} {: 10.5f}'.format(arep2, prep2))
            outfile.write('{: 10.5f} {: 10.5f}'.format(aimp2, pimp2))

        fitrep1 = fitimp1 = fitrep2 = fitimp2 = 0

        # Calculate std. dev before subtraction
        #prerep1 = np.std(datrep1)
        #preimp1 = np.std(datimp1)
        #prerep2 = np.std(datrep2)
        #preimp2 = np.std(datimp2)

        # Get robust mean and standard deviation
        prerep1 = stats.sigmaclipstats(datrep1)[1:3]
        preimp1 = stats.sigmaclipstats(datimp1)[1:3]
        prerep2 = stats.sigmaclipstats(datrep2)[1:3]
        preimp2 = stats.sigmaclipstats(datimp2)[1:3]

        if arep1 > 0:  #prerep1[0] + 1*prerep1[1]:
            fitrep1 = gensin(srclmn[ss], (baseu, basev, basew), arep1, prep1)
        if aimp1 > 0:  #preimp1[0] + 1*preimp1[1]:
            fitimp1 = gensin(srclmn[ss], (baseu, basev, basew), aimp1, pimp1)
        if arep2 > 0:  #prerep2[0] + 1*prerep2[1]:
            fitrep2 = gensin(srclmn[ss], (baseu, basev, basew), arep2, prep2)
        if aimp2 > 0:  #preimp2[0] + 1*preimp2[1]:
            fitimp2 = gensin(srclmn[ss], (baseu, basev, basew), aimp2, pimp2)

        # Get robust mean and standard deviation
        postrep1 = stats.sigmaclipstats(datrep1 - fitrep1)[1:3]
        postimp1 = stats.sigmaclipstats(datimp1 - fitimp1)[1:3]
        postrep2 = stats.sigmaclipstats(datrep2 - fitrep2)[1:3]
        postimp2 = stats.sigmaclipstats(datimp2 - fitimp2)[1:3]

        #postrep1 = np.std(datrep1 - fitrep1)
        #postimp1 = np.std(datimp1 - fitimp1)
        #postrep2 = np.std(datrep2 - fitrep2)
        #postimp2 = np.std(datimp2 - fitimp2)

        if outfile is not None:
            outfile.write('{: 10.5f} {: 10.5f}'.format(postrep1[1],
                                                       postimp1[1]))
            outfile.write('{: 10.5f} {: 10.5f}'.format(postrep2[1],
                                                       postimp2[1]))
            outfile.write('\n')

        if postrep1[1] <= prerep1[1]:
            modeldat[nn, :, 0, 0] += fitrep1
        if postimp1[1] <= preimp1[1]:
            modeldat[nn, :, 0, 1] += fitimp1
        if postrep2[1] <= prerep2[1]:
            modeldat[nn, :, 1, 0] += fitrep2
        if postimp2[1] <= preimp2[1]:
            modeldat[nn, :, 1, 1] += fitimp2

        if doplot and arep1 > 0:
            cumlfit += fitrep1

    if doplot and np.count_nonzero(cumlfit):
        print("Baseline {} Channel {} Source {}".format(
            bb + 1, nn + 1, ss + 1))


        date = Time(date, format='jd')
        date = date.datetime

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
        plt.style.use('seaborn-poster')

        fig, ax = plt.subplots()

        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)

        ax.plot(date, datrep1, '-o', label='Data')
        ax.plot(date, cumlfit, label='Fit')

        ax.set_xlabel('Time (hours)', fontsize=30)
        ax.set_ylabel('Amplitude (Jy)', fontsize=30)

        ax.tick_params(which='major', length=10, labelsize=30)
        ax.tick_params(which='minor', length=5, labelsize=30)

        ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        ax.xaxis_date()

        plt.legend(fontsize=30)
        plt.tight_layout()

        if savepath is not None:
            plt.savefig(savepath + '.png', bbox_inches='tight')
            plt.savefig(savepath + '.pdf', bbox_inches='tight')
        else:
            plt.show()



def _text_file_header(outfile):
    """
    Print the header of the output text file.
    """

    outfile.write('Base Chan  date src  AmpReP1    PhsReP1   AmpImP1    ')
    outfile.write('PhsImP1   AmpReP2    PhsReP2   AmpImP2    PhsImP2   ')
    outfile.write('RmsReP1    RmsImP1   RmsReP2    RmsImP')
    outfile.write('\n')


def defringe(fitsfile, phcentre, srclist, solint, uvlim, do_model, outfile,
             amplim, doplot, savepath):
    """
    Reads the FITS file and the source list and fits a fringe per
    source defined in the source list and subtracts it baseline-by-baseline
    from the FITS file.

    Input:
        fitsfile    Name of the input FITS file, str
        phcentre    Coordinates of the phase centre, astropy SkyCoord object
        srclist     Name of the file containing the source coordinates, str
        solint      Solution interval to fit within, float
        uvlim       Lower uv limit below which defringing will not be run.
        do_model    Write out the model to a UV fits file for later analysis.

    Returns:
        None - The modifications are done in place to the FITS file
    """

    import jansky.lib.const as const
    import jansky.lib.sky as sky
    import jansky.lib.antennas as antennas
    import shutil

    from itertools import starmap, repeat
    from sys import stderr
    from astropy.time import Time

    fitspars = jfits.fitspars(fitsfile)
    gcount = fitspars.gcount

    integration, __, jitter = jfits.fitsintegration(fitsfile)
    ngroup = int(solint // integration)
    ngroup = int(ngroup * const.NBASELINE)

    # Get source information
    poslist = get_source_pos(srclist)
    # Convert from RA/DEC to lmn
    srclmn = [sky.radec_to_lmn(phcentre, position) for position in poslist]

    nsource = len(srclmn)

    strings.clearline()
    if do_model == True:  # Copy input file to write model into
        print('Copying input FITS file to new MODEL file', end='\r')
        modelfile = fitsfile.upper().strip('.FITS') + '_MODEL.FITS'
        shutil.copyfile(fitsfile, modelfile)

        strings.clearline()
        print('Resetting MODEL file', end='\r')
        jfits.reset_fits(modelfile)

        mfile = fits.open(modelfile, memmap=True, mode='update')

    # Open FITS file
    ffile = fits.open(fitsfile, memmap=True, mode='update')

    uscale = ffile[0].header['PSCAL1']
    vscale = ffile[0].header['PSCAL2']
    wscale = ffile[0].header['PSCAL3']

    # UVW values will change by a factor freqfactor per channel
    freqfactor = np.array(1. + (
        fitspars.chanwidth * np.arange(fitspars.nchan) / fitspars.refreq))

    strings.clearline()

    if outfile is not None:
        _text_file_header(outfile)

    # Calculate scan start and scan end groups
    group_beg, group_end = jfits.scan_break(fitsfile)

    group = 0
    for beg, end in zip(group_beg, group_end):
        print("Processing group", beg, "/", gcount, end="\r", file=stderr)

        imgdat = np.squeeze(ffile[0].data[beg:end + 1].data)
        basepar = ffile[0].data[beg:end + 1].par('BASELINE')
        uval = ffile[0].data[beg:end + 1].par(0) / uscale
        vval = ffile[0].data[beg:end + 1].par(1) / vscale
        wval = ffile[0].data[beg:end + 1].par(1) / wscale

        if outfile is not None or doplot:
            date = ffile[0].data[beg:end + 1].par('DATE')
            #date = Time(date, format='jd')

        uvval = np.hypot(uval, vval)

        if do_model == True:
            moddat = np.squeeze(mfile[0].data[beg:end + 1].data)

        # If baselines are in AIPS format, change them
        # TODO:This only works because the FITS file is 'complete'. Figure out
        # a more robust check.
        if np.any(basepar[0:10] % 256 > 0):
            basepar = antennas.base_aips_seq(basepar, doself=False)

        last_iter = False
        for gg in range(0, end - beg, ngroup):
            if last_iter:
                continue

            resid = end - (gg + beg + ngroup)
            # If there is little bit remaining after the last chunk include it
            if resid < ngroup:
                chunk = slice(gg, None)
                last_iter = True
            else:
                chunk = slice(gg, gg + ngroup)

            subuv = uvval[chunk]
            subbase = basepar[chunk]
            subimg = imgdat[chunk]

            if do_model == True:
                submod = moddat[chunk]

            if outfile is not None or doplot:
                subdate = date[chunk]

            # Defringe every channel of every baseline
            for bb in range(int(const.NBASELINE)):
                # Select only those times where the baseline has uv > uvlim
                indices = np.where((subuv > uvlim) & (subbase == bb + 1))

                # Not enough points to fit the fringe
                if np.array(indices).shape[1] <= 10:
                    continue

                basedat = subimg[indices]
                # Swap time and channel, make channel the zero-th axis
                basedat = np.swapaxes(basedat, 0, 1)
                modeldat = np.zeros_like(basedat)

                if do_model == True:
                    basemod = submod[indices]
                    basemod = np.swapaxes(basemod, 0, 1)

                if outfile is not None or doplot:
                    basedate = subdate[indices]
                else:
                    basedate = 0.

                uvw = [uval[indices], vval[indices], wval[indices]]

                for nn in range(fitspars.nchan):
                    #if outfile is not None:
                    #    outfile.write('{: 04d} {: 04d}'.format(bb+1, nn+1))
                    #    outfile.write('{: 08f}'.format(np.median(basedate)))

                    subtract_channel(uvw, freqfactor[nn], bb, basedat, nsource,
                                     srclmn, nn, modeldat, outfile, amplim,
                                     basedate, doplot, savepath)

                basedat -= modeldat
                # Unswap the axes
                basedat = np.swapaxes(basedat, 0, 1)
                subimg[indices] = basedat

                if do_model == True:
                    basemod += modeldat
                    basemod = np.swapaxes(basemod, 0, 1)
                    submod[indices] = basemod

            imgdat[chunk] = subimg

            if do_model == True:
                moddat[chunk] = submod

        writedat = imgdat[:, np.newaxis, np.newaxis, np.newaxis, ...]
        ffile[0].data[beg:end + 1].base['DATA'] = writedat

        if do_model == True:
            writemod = moddat[:, np.newaxis, np.newaxis, np.newaxis, ...]
            mfile[0].data[beg:end + 1].base['DATA'] = writemod
            mfile.flush()

        ffile.flush()

    ffile.close()

    if do_model == True:
        mfile.close()

    if outfile is not None:
        outfile.close()


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('fitsfile', type=click.Path(exists=True))
@click.argument('ph_ra')
@click.argument('ph_dec')
@click.argument('srclist', type=click.Path(exists=True))
@click.option(
    '--solint',
    '-s',
    type=float,
    default=300,
    help='Solution interval in seconds  [default: 300]')
@click.option(
    '--uvlim',
    type=float,
    default=100,
    help='Lower uv limit (in lambda) below which source will not be '
    'subtracted [default: 100]')
@click.option(
    '--model',
    'do_model',
    is_flag=True,
    help='Write out fitted model to a UV FITS file.')
@click.option(
    '--outfile',
    type=click.File(mode='w'),
    help='Write out fitted amplitudes and phases to a text file')
@click.option(
    '--amplim',
    type=float,
    default=0.1,
    help='Minimum amplitude of fitted component in Jy  (default:0.1)')
@click.option(
    '--doplot',
    is_flag=True,
    help='Plot the cumulative fit and residuals per channel')
@click.option(
    '--savepath',
    type=click.Path(),
    help=
    'Path to which to save file, will save PNG and PDF; '
    'Only if doplot is True')
def main(fitsfile, ph_ra, ph_dec, srclist, solint, uvlim, do_model, outfile,
         amplim, doplot, savepath):
    """
    Given an input FITSFILE containing visibility data of a source centred at
    (PH_RA, PH_DEC), this program runs baseline based defringing to correct for
    direction dependent effects in the direction of the sources provided in
    SRC_LIST.

    PH_RA & PH_DEC should be in the format HH:MM:SS and DD:MM:SS respectively.
    """

    # Require a complete file - Will probably remove this condition later
    jfits.checkcomplete(fitsfile)

    phcentre = strings.string_to_coord(ph_ra, ph_dec)
    defringe(fitsfile, phcentre, srclist, solint, uvlim, do_model, outfile,
             amplim, doplot, savepath)
