"""
Plotting routines for text files. Uses pandas to read in CSV and text files
and visualize them in various ways.
"""

import click
import pandas as pd

CONTEXT_SETTINGS=dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    Command line tools to visualize text files (CSV/ASCII).
    """
    pass


def _read_file(textfile, header, sep):
    """
    Worker function to read in the text file, and return the pandas dataframe
    """
    if header:
        textfile.seek(0)
        for line in textfile:
            if line[0] == '#':
                line = line.lstrip('#')
                names = line.split()
            else:
                names = line.split()
        textfile.seek(0)
    else:
        names = None

    if sep is None:
        df = pd.read_csv(textfile, delim_whitespace=True, header=None,
                names=names)
    else:
        df = pd.read_csv(textfile, sep=sep, header=None, names=names)

    return df


@main.command(short_help='Plot histograms of various columns of a text file')
@click.argument('textfile', type=click.File())
@click.option('--rows', help='Select rows to plot')
@click.option('--cols', help='Select columns to plot')
@click.option('--sep',help="Specify separator (ex: ','). Default is whitespace")
@click.option('--no-header', 'header', is_flag=True,
        help='Specify that there is no header for the input file')
@click.option('--stacked', is_flag=True,
        help='Plot the histogram of each column separately and stack them')
@click.option('--nbin', default=100,
        help='Number of bins in histogram  [default:100]')
def hist(textfile, rows, cols, sep, header, stacked, nbin):
    """
    Given an input TEXTFILE, plot a histogram of it's contents.

    The rows and columns to be plotted from the text file can be specified
    using the python slicing notation. Row numbers and column numbers are zero
    indexed, and the last element of the slice is non inclusive.

    Ex: Specifying --rows '0:8' will select rows 0 through 7.
                   --rows '0:8:2' will select every second row between 0 and 7.

    The default separator is whitespace - This will match with any whitespace.
    But the separator for the text file can be passed in through the --sep
    option.
    """

    import numpy as np
    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    from jansky.lib.strings import getslice

    df = _read_file(textfile, header, sep)

    if rows is not None:
        row_slice = getslice(rows)
    if cols is not None:
        col_slice = getslice(cols)

    if rows is not None and cols is not None:
        df = df.iloc[row_slice, col_slice]
    elif rows is not None and cols is None:
        df = df.iloc[row_slice]
    elif cols is not None and rows is None:
        df = df.iloc[:, col_slice]

    if stacked is True:
        df.plot.hist(bins=nbin, alpha=0.5)

    if stacked is False:
        hist, bins = np.histogram(df, bins=nbin)
        centre = (bins[:-1] + bins[1:])/2.
        width = 1.0 * (bins[1] - bins[0])
        plt.bar(centre, hist, align='center', width=width, alpha=0.5)

    plt.show()



@main.command(short_help='Plot line plots of various columns of a text file')
@click.argument('textfile', type=click.File())
@click.argument('cols', required=False)
@click.option('--sep',help="Specify separator (ex: ','). Default is whitespace")
@click.option('--no-header', 'header', is_flag=True,
        help='Specify that there is no header for the input file')
def line(textfile, cols, sep, header):
    """
    Given an input TEXTFILE, plot all the columns in the text file
    (if COLS is not specified) else plot the selection of columns defined
    by COLS.

    COLS can be specified using the Python array slicing notation.

    For example specifying,
    0:8 will plot the first through seventh columns.
    0:8:2 will plot every second row between the first and seventh columns.

    The default separator is whitespace - This will match with any whitespace.
    But the separator for the text file can be passed in through the --sep
    option.
    """

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    from jansky.lib.strings import getslice

    df = _read_file(textfile, header, sep)

    if cols is not None:
        cols_slice = getslice(cols)
        df = df.iloc[:, cols_slice]

    df.plot()
    plt.tight_layout()
    plt.show()
