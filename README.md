# Python for radio astronomy

Python scripts to visualize and analyze radio interferometric data - Mostly 
specific to the GMRT. 

It makes extensive use of the existing scientific python environment, so it is
highly recommended to install the Anaconda python distribution and keep it 
updated before running any of these scripts.

These scripts are all written in Python 3 - Compatibility with Python 2 is not
guaranteed, and is not attempted either.
